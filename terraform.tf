# https://thegeeklab.de/posts/2022/09/store-terraform-state-on-backblaze-s3/
terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }

    tailscale = {
      source  = "tailscale/tailscale"
      version = "0.13.7"
    }
  }

  # init using: tfi -backend-config=path/to/decrypted/infra-backend
  backend "s3" {
    bucket                      = "tfinfra"
    key                         = "tf-infra.tfstate"
    skip_credentials_validation = true
    skip_region_validation      = true
    # endpoint                    = var.b2_endpoint
    # region                      = var.b2_region
    # access_key                  = var.b2_access_key
    # secret_key                  = var.b2_secret_key
    # encrypt = true
    # openssl rand -base64 32
    # sse_customer_key = "fsRb1SXBjiUqBM0rw/YqvDixScWnDCZsK7BhnPTc93Y="
  }
}
