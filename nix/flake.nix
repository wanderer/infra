{
  description = "NixOS configuration for all the things (as many as we can get)";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.home-manager = {
    url = "github:nix-community/home-manager";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  inputs.disko.url = "github:nix-community/disko";
  inputs.disko.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  inputs.agenix.url = "github:ryantm/agenix";
  inputs.agenix.inputs.nixpkgs.follows = "nixpkgs";
  inputs.sops-nix.url = "github:Mic92/sops-nix";
  inputs.attic.url = "github:zhaofengli/attic";
  inputs.authentik-nix.url = "github:mayflower/authentik-nix";

  outputs = inputs @ {
    self,
    nixpkgs,
    home-manager,
    disko,
    nixos-hardware,
    agenix,
    sops-nix,
    attic,
    authentik-nix,
    ...
  }: let
    projname = "nix-infra";
    # nix.registry.nixpkgs.flake = nixpkgs;
    system = "x86_64-linux";
    supportedSystems = ["x86_64-linux" "aarch64-linux"];
    # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    # Nixpkgs instantiated for supported system types.
    nixpkgsFor = forAllSystems (system:
      import nixpkgs {
        inherit system;
        overlays = [
          # no overlay imports atm
        ];
      });
    # pkgs = nixpkgs.legacyPackages.${system};
    pkgs = nixpkgsFor.${system};

    inherit (nixpkgs.lib) nixosSystem;
  in {
    formatter = forAllSystems (
      system:
        nixpkgsFor.${system}.alejandra
    );
    # formatter.${system} = pkgs.alejandra;

    nixosConfigurations.loki = nixpkgs.lib.nixosSystem {
      # inherit pkgs system;
      modules = [
        disko.nixosModules.disko
        agenix.nixosModules.default
        sops-nix.nixosModules.sops
        attic.nixosModules.atticd
        authentik-nix.nixosModules.default

        ./hosts/loki/configuration.nix
      ];
    };

    nixosConfigurations.t14 = nixpkgs.lib.nixosSystem {
      # inherit pkgs system;
      modules = let
        usr = "mko";
      in [
        nixos-hardware.nixosModules.lenovo-thinkpad-t14-amd-gen2
        disko.nixosModules.disko
        sops-nix.nixosModules.sops

        ./hosts/t14/configuration.nix
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.extraSpecialArgs = {
            inherit usr;
            hostName = "t14";
          };
          home-manager.users.${usr} = {
            home.stateVersion = "23.11";
            imports = [
              ./hosts/t14/home.nix
            ];
          };
        }
      ];
    };

    nixosConfigurations.monoceros = nixpkgs.lib.nixosSystem {
      # inherit pkgs system;
      modules = [
        disko.nixosModules.disko
        sops-nix.nixosModules.sops

        ./hosts/monoceros/configuration.nix
      ];
    };

    nixosConfigurations.nixpi = nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      # pkgs = nixpkgs.legacyPackages.${system};
      # pkgs = nixpkgsFor.${system};
      modules = [
        sops-nix.nixosModules.sops

        ./hosts/nixpi/configuration.nix
      ];
    };
    devShells = forAllSystems (
      system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
          ];
        };
      in {
        default = with pkgs;
          mkShell
          {
            name = "${projname}";

            shellHook = ''
              echo " -- in ${projname} dev shell..."
            '';

            nativeBuildInputs = [
            ];
            packages =
              [cachix]
              ++ (
                if stdenv.isLinux
                then [
                ]
                else []
              );
          };
      }
    );
  };
}
