{config, ...}: let
  svc = "coredns.service";
  usr = "${toString config.users.users.coredns.name}";
in {
  imports = [../../../modules/coredns.nix];

  sops.secrets = {
    "coredns/ifaces".restartUnits = [svc];
    "coredns/iptailscale".restartUnits = [svc];
    "coredns/ifaces".owner = usr;
    "coredns/iptailscale".owner = usr;
  };

  systemd.services.coredns = {
    wants = ["dnscrypt-proxy2.service"];
  };
}
