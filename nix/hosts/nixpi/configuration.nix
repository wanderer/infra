{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    ./modules/coredns.nix

    ../../modules/base.nix
    ../../modules/dnscrypt.nix
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age = {
      keyFile = "/root/.age/nixpi-key";
      sshKeyPaths = ["/root/.ssh/nixpiage" "/etc/ssh/ssh_host_ed25519_key"];
      generateKey = false;
    };

    secrets.rootPassphrase.owner = "root";
    # secrets.domainName.restartUnits = ["caddy.service" "coredns.service"];
    # secrets.domainName.restartUnits = ["coredns.service"];
  };

  nixpkgs = {
    buildPlatform.system = "x86_64-linux";
    hostPlatform.system = "aarch64-linux";
  };
  boot = {
    # kernelPackages = pkgs.linuxKernel.packages.linux_rpi3;

    kernelPackages = pkgs.linuxPackages_latest;

    # initrd.availableKernelModules = ["xhci_pci" "usbhid" "usb_storage"];
    initrd.availableKernelModules = ["usbhid"];

    loader = {
      grub.enable = false;
      # systemd-boot = {
      # enable = true;
      # configurationLimit = 12;  # maximum number of latest NixOS generations to show
      # };
      generic-extlinux-compatible.enable = true;

      # raspberryPi.firmwareConfig = lib.mkForce ''
      # gpu_mem=256
      # '';
      # kernelParams = ["cma=256M"];
    };
  };
  powerManagement.cpuFreqGovernor = "ondemand";

  hardware.bluetooth.enable = true;
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = ["noatime"];
    };
  };

  networking = {
    hostName = "nixpi";
    wireless = {
      enable = true;
      # networks."${SSID}".psk = SSIDpassword;

      # interfaces = [ interface ];
    };
  };

  documentation.nixos.enable = false;

  environment.systemPackages = with pkgs; [
    vim
    zsh
    raspberrypifw
    neofetch
  ];

  services.openssh.enable = true;

  programs.zsh.enable = true;
  users.users.root = {
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBtG6NCgdLHX4ztpfvYNRaslKWZcl6KdTc1DehVH4kAL"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaXmXbNegxiXLldy/sMYX8kCsghY1SGqn2FZ5Jk7QJw"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBZbkw9vjCfbMPEH7ZAFq20XE9oIJ4w/HRIMu2ivNcej caelum's nixbldr key"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGKzPC0ZK4zrOEBUdu1KNThEleVb1T5Pl3+n3KB3o0b8 surtur's nixbldr key"
    ];
    hashedPasswordFile = config.sops.secrets.rootPassphrase.path;

    subUidRanges = [
      {
        count = 65536;
        startUid = 65536 * 28; # 1835008, docker
      }
    ];
  };

  services = {
    #prometheus = {
    #  # WIP.
    #  enable = true;
    #  # openFirewall = true;
    #  port = 9090;
    #  exporters = {
    #    node = {
    #      enable = true;
    #      enabledCollectors = [
    #        "logind"
    #        "systemd"
    #      ];
    #      port = 9100;
    #    };
    #  };

    #  scrapeConfigs = [
    #    {
    #      job_name = "node";
    #      static_configs = [
    #        {
    #          targets = [
    #            "nixpi.local:${toString config.services.prometheus.exporters.node.port}"
    #          ];
    #        }
    #      ];
    #    }
    #  ];
    #};
  };

  hardware.enableRedistributableFirmware = true;

  # system.stateVersion = "23.11";
}
