{
  config,
  lib,
  pkgs,
  usr,
  hostName,
  ...
}: let
  dotfiles = builtins.fetchGit {
    url = "https://git.dotya.ml/wanderer/dotfiles";
    rev = "a08927ef24a1fa7b408f6e3bec5754cb733a9c49";
  };
  swayTgt = "sway-session.target";
  start-waybar = pkgs.writeShellScriptBin "start-waybar" ''
    export SWAYSOCK=/run/user/$(id -u)/sway-ipc.$(id -u).$(pgrep -f 'sway$').sock
    ${pkgs.waybar}/bin/waybar
  '';
  start-autotiling = pkgs.writeShellScriptBin "start-autotiling" ''
    export SWAYSOCK=/run/user/$(id -u)/sway-ipc.$(id -u).$(pgrep -f 'sway$').sock
    ${pkgs.autotiling}/bin/autotiling
  '';
in {
  imports = [
    # ./modules/desktop/waybar.nix
    # ./modules/desktop/wayland.nix
    ./modules/desktop/sway.nix
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age.keyFile = "/run/secrets-rendered/sopsFile";
    secrets = {
      gitUsername.path = "%r/secrets/gitUsername";
      gitConf.path = "${config.home.homeDirectory}/.config/git/config.${hostName}";
    };
  };

  # home.enableNixpkgsReleaseCheck = false;
  home = {
    username = lib.mkForce "${usr}";
    homeDirectory = lib.mkForce "/home/${usr}";

    packages = with pkgs; [
      actionlint
      asciinema
      asciiquarium
      bat
      bc
      black
      sqlite
      bun
      caddy
      cliphist
      cowsay
      curlie
      dig
      dive
      dogdns
      doggo
      entr
      eza
      exif
      exiftool
      fd
      feh
      ffmpeg
      figlet
      file
      firefox-wayland
      chromium
      fx
      gcc
      glow
      gnumake
      go_1_21
      gofumpt
      golangci-lint
      golangci-lint-langserver
      gopls
      goreleaser
      gotools
      graph-easy
      hadolint
      hugo
      ipcalc
      jq
      killall
      libnotify
      libtelnet
      lolcat
      lsof
      mpv-unwrapped
      neofetch
      netcat
      networkmanagerapplet
      nixfmt
      nmap
      nodePackages.prettier
      nodePackages.typescript
      nodePackages.typescript-language-server
      nodePackages.yaml-language-server
      nodejs
      pciutils
      pgcli
      pulsemixer
      pylint
      python3
      python39Packages.pipx
      ripgrep
      rnix-lsp
      rsync
      shellcheck
      shfmt
      signal-desktop
      # slack
      statix
      p7zip
      unzip
      gnome.nautilus
      gnome.seahorse
      gnome.eog
      # tailscale-systray
      yaru-theme
      junction
      usbutils
      v4l-utils
      wev
      whois
      xdg-utils
      youtube-dl
      yq-go
      zathura
      zip
    ];

    stateVersion = lib.mkForce "23.11";
  };

  dconf.enable = true;
  #dconf.settings = {
  #  "org/gnome/desktop/interface" = {
  #    color-scheme = "prefer-dark";
  #  };
  #};
  #gtk = {
  #  enable = true;

  #  iconTheme = {
  #    name = "Papirus-Dark";
  #    package = pkgs.papirus-icon-theme;
  #  };

  #  theme = {
  #    name = "palenight";
  #    package = pkgs.palenight-theme;
  #  };

  #  cursorTheme = {
  #    name = "Numix-Cursor";
  #    package = pkgs.numix-cursor-theme;
  #  };

  #  gtk3.extraConfig = {
  #    Settings = ''
  #      gtk-application-prefer-dark-theme=1
  #    '';
  #  };

  #  gtk4.extraConfig = {
  #    Settings = ''
  #      gtk-application-prefer-dark-theme=1
  #    '';
  #  };
  #};
  home.sessionVariables.GTK_THEME = "Dracula";

  editorconfig.settings = {
    "*" = {
      charset = "utf-8";
      end_of_line = "lf";
      trim_trailing_whitespace = true;
      insert_final_newline = true;
      max_line_width = 78;
      indent_style = "space";
      indent_size = 4;
    };
  };

  services = {
    blueman-applet.enable = true;
    network-manager-applet.enable = true;

    avizo.enable = true;

    batsignal = {
      enable = true;
      extraArgs = ["-w 25" "-c 15" "-d 10"];
    };
  };
  systemd.user.services.avizo.Service = {
    RestartSec = "5s";
    Restart = "always";
  };

  programs = {
    #fzf = {
    #  enable = true;
    #  enableZshIntegration = true;
    #};
    gh = {enable = true;};
    home-manager = {enable = true;};
    obs-studio = {
      enable = true;
      plugins = with pkgs.obs-studio-plugins; [wlrobs];
    };
  };

  # https://rycee.gitlab.io/home-manager/options.html#opt-programs.ssh.matchBlocks

  home.file = {
    ".config/kitty/kitty.conf" = {
      source = "${dotfiles}/.config/kitty/kitty.conf";
    };

    ".vimrc" = {
      source = "${dotfiles}/.vim/vimrc";
    };
    ".vim/deoplete.vimrc.vim" = {
      source = "${dotfiles}/.vim/deoplete.vimrc.vim";
    };
    ".vim/gotags.vimrc.vim" = {
      source = "${dotfiles}/.vim/gotags.vimrc.vim";
    };
    ".vim/python.vimrc.vim" = {
      source = "${dotfiles}/.vim/python.vimrc.vim";
    };

    ".config/nvim/init.vim" = {
      source = "${dotfiles}/.config/nvim/init.vim";
    };

    ".config/systemd/user.conf" = {
      text = ''
        [Manager]
        DefaultTimeoutStarSec=15s
        DefaultTimeoutStopSec=15s
      '';
    };

    ".config/sway/config.d/dracula.sway" = {
      source = "${dotfiles}/.config/sway/config.d/dracula.sway";
    };
    ".config/waybar/config" = {
      source = "${dotfiles}/.config/waybar/config";
    };
    ".config/waybar/style.css" = {
      source = "${dotfiles}/.config/waybar/style.css";
    };
    ".config/waybar/modules/storage.sh" = {
      source = "${dotfiles}/.config/waybar/modules/storage.sh";
    };
    ".config/kanshi/config" = {
      source = "${dotfiles}/.config/kanshi/config";
    };

    ".config/mako/config" = {
      source = "${dotfiles}/.config/mako/config";
    };
    ".config/swaylock/config" = {
      source = "${dotfiles}/.config/swaylock/config";
    };

    ".config/albert.conf" = {
      source = "${dotfiles}/.config/albert/albert.conf";
    };

    ".config/direnv/direnv.toml" = {
      source = "${dotfiles}/.config/direnv/direnv.toml";
    };

    ".config/git/config-common" = {
      source = "${dotfiles}/.config/git/config";
    };
    # host-specific gitconfig.
    # ".config/git/config.${hostName}" = {
    #text = ''
    #  [user]
    #      name = ${hostName}
    #      email = wanderer@dotya.ml
    #      signingkey = ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDAqwhsHHLKG7k7kDa7Tauy2V+rXqQFxiuMCdVltz0n1
    #'';
    # }
    # source = config.sops.secrets.gitConf;
    # text = ''
    #   [user]
    #       name = ${hostName}
    #       email = ${config.sops.secrets.gitUsername}
    #       signingkey = ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDAqwhsHHLKG7k7kDa7Tauy2V+rXqQFxiuMCdVltz0n1
    # '';
    # };
    ".config/git/config" = {
      text = ''
        [include]
          path = ~/.config/git/config-common
        [include]
          path = ~/.config/git/config.${hostName}
      '';
    };
    ".config/git/allowed_signers" = {
      source = "${dotfiles}/.config/git/allowed_signers";
    };

    ".cargo/config.toml" = {
      source = "${dotfiles}/.cargo/config.toml";
    };

    ".npmrc" = {
      text = ''
        prefix=''${HOME}/.npm-packages
        audit=false
        fund=false
      '';
    };

    ".config/tridactyl/tridactylrc" = {
      source = "${dotfiles}/.config/tridactyl/tridactylrc";
    };

    ".config/bat/config" = {
      source = "${dotfiles}/.config/bat/config";
    };

    ".ncpamixer.conf" = {
      source = "${dotfiles}/.config/ncpamixer.conf";
    };

    ".gdbinit" = {
      text = ''
        set auto-load safe-path /nix/store
        set history save on
        set history size 10000
        set history remove-duplicates 100
        set history filename ~/.gdb_history
      '';
    };

    #".local/bin/winprint.sh" = {
    #  source = "${dotfiles}/bin/winprint.sh";
    #  executable = true;
    #};
  };

  systemd.user.services = {
    waybar = {
      Unit = {
        Description = "Highly customizable Wayland bar for Sway and Wlroots based compositors.";
        Documentation = "https://github.com/Alexays/Waybar/wiki/";
        PartOf = swayTgt;
        After = swayTgt;
      };

      Service = {
        ExecStart = "${start-waybar}/bin/start-waybar";
        ExecReload = "kill -SIGUSR2 $MAINPID";
        Restart = "on-failure";
        RestartSec = "3s";
        LockPersonality = true;
        PrivateTmp = "yes";
        DevicePolicy = "closed";
      };

      Install = {WantedBy = [swayTgt];};
    };

    #autotiling = {
    #  Unit = {
    #    Description = "Script for sway and i3 to automatically switch the horizontal / vertical window split orientation";
    #    Documentation = "https://github.com/nwg-piotr/autotiling";
    #    BindsTo = swayTgt;
    #  };

    #  Service = {
    #    Type = "simple";
    #    ExecStart = "${start-autotiling}/bin/start-autotiling";
    #    Restart = "always";
    #    RestartSec = "5s";
    #    LockPersonality = true;
    #    PrivateTmp = "yes";
    #    DevicePolicy = "closed";
    #  };

    #  Install = {WantedBy = [swayTgt];};
    #};

    # albert = {
    #   Unit = {
    #     Description = "A C++/Qt based desktop agnostic keyboard launcher that helps you to accomplish your workflows in a breeze";
    #     Documentation = "https://albertlauncher.github.io/";
    #     BindsTo = swayTgt;
    #   };

    #   Service = {
    #     Type = "simple";
    #     # after hm stateVersion bump to 23.05, albert doesn't seem to support explicit wayland.
    #     Environment = "QT_QPA_PLATFORM=xcb";
    #     ExecStart = "${pkgs.albert}/bin/albert";
    #     Restart = "always";
    #     RestartSec = "3s";
    #     LockPersonality = true;
    #     PrivateTmp = "yes";
    #     DevicePolicy = "closed";
    #   };

    #   Install = {WantedBy = [swayTgt];};
    # };

    mako = {
      Unit = {
        Description = "Mako notification daemon";
        BindsTo = [swayTgt];
      };
      Install = {
        WantedBy = [swayTgt];
      };
      Service = {
        Type = "dbus";
        BusName = "org.freedesktop.Notifications";
        ExecStart = "${pkgs.mako}/bin/mako";
        RestartSec = 5;
        Restart = "always";
      };
    };

    gopls = {
      Unit = {
        Description = "Go language server";
      };

      Service = {
        Type = "simple";
        ExecStartPre = "bash -c 'rm -v -f /tmp/.gopls-daemon.sock || true'";
        ExecStart = "%h/go/bin/gopls -listen='unix;/tmp/.gopls-daemon.sock'";
        ExecStopPost = "bash -c 'rm -v -f /tmp/.gopls-daemon.sock || true'";
        Restart = "on-failure";
        RestartSec = "1m";
        TimeoutStopFailureMode = "abort";
        SystemCallFilter = "~@reboot @obsolete";
        ProtectProc = "invisible";
        ProcSubset = "pid";
        ProtectHome = "tmpfs";
        BindPaths = ["%h/.cache"];
        RestrictNamespaces = true;
        NoNewPrivileges = "yes";
        ProtectSystem = "strict";
        DevicePolicy = "closed";
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        #RestrictAddressFamilies="AF_UNIX AF_INET AF_INET6";
        RestrictAddressFamilies = "AF_UNIX";
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
      };

      Install = {WantedBy = ["default.target"];};
    };
  };

  xdg = (import "${dotfiles}/nix/xdg.nix") {inherit pkgs config hostName;};
}
