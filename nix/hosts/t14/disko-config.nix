{
  config,
  disks ? ["/dev/nvme0n1"],
  lib,
  ...
}: {
  disko.devices = {
    disk = {
      nvme0n1 = {
        type = "disk";
        device = "/dev/nvme0n1";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              type = "EF00";
              size = "1G";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
          };
        };
      };
    };
    zpool = {
      zroot = {
        type = "zpool";
        mode = ""; # == single disk
        options = {
          ashift = "12";
          autotrim = "on";
        };
        rootFsOptions = {
          encryption = "aes-256-gcm";
          keyformat = "passphrase";
          keylocation = "file:///root/.zfs-zroot-pool.key";
          #postCreateHook = ''
          #  "zfs change-key -o keylocation=prompt zroot/$name"
          #'';
          checksum = "sha512";
          atime = "off";
          compression = "zstd";
          "com.sun:auto-snapshot" = "false";
        };
        mountpoint = null;
        postCreateHook = "zfs snapshot zroot@blank";

        datasets = {
          # can be rebuilt.
          local = {
            type = "zfs_fs";
            mountpoint = null;
            options."com.sun:auto-snapshot" = "false";
          };
          "local/nix" = {
            type = "zfs_fs";
            mountpoint = "/nix";
          };
          "system" = {
            type = "zfs_fs";
            mountpoint = null;
            options = {
              "com.sun:auto-snapshot" = "false";
            };
          };
          # system root.
          "system/nixos" = {
            type = "zfs_fs";
            mountpoint = "/";
            options = {
              "com.sun:auto-snapshot" = "true";
            };
          };
          "system/nixos/var" = {
            type = "zfs_fs";
            mountpoint = "/var";
            options = {
              "com.sun:auto-snapshot" = "true";
            };
          };
          "system/nixos/var/log" = {
            type = "zfs_fs";
            mountpoint = "/var/log";
            options = {
              "com.sun:auto-snapshot" = "true";
            };
          };
          "system/nixos/var/cache" = {
            type = "zfs_fs";
            mountpoint = "/var/cache";
            options = {
              "com.sun:auto-snapshot" = "true";
            };
          };
          "system/nixos/var/containers-storage" = {
            type = "zfs_fs";
            mountpoint = "/var/lib/containers/storage";
            options = {
              "com.sun:auto-snapshot" = "false";
              acltype = "posixacl";
            };
          };
          # frequently snapshot and backed up data.
          userdata = {
            type = "zfs_fs";
            mountpoint = "/DATA";
            options = {
              # encryption = "aes-256-gcm";
              # keyformat = "passphrase";
              # keylocation = "file:///root/.zfs-DATA.key";
              "com.sun:auto-snapshot" = "false";
            };
          };
          "userdata/home" = {
            type = "zfs_fs";
            mountpoint = "/DATA/home";
            options = {
              # encryption = "aes-256-gcm";
              # keyformat = "passphrase";
              # keylocation = "file:///root/.zfs-DATA-home.key";
              "com.sun:auto-snapshot" = "true";
            };
          };
          "userdata/home/root" = {
            type = "zfs_fs";
            mountpoint = "/root";
            options = {
              "com.sun:auto-snapshot" = "true";
            };
          };
          "userdata/home/root-cache" = {
            type = "zfs_fs";
            mountpoint = "/root/.cache";
            options."com.sun:auto-snapshot" = "false";
          };
          "userdata/home/root-config" = {
            type = "zfs_fs";
            mountpoint = "/root/.config";
            options = {
              "com.sun:auto-snapshot" = "true";
            };
          };
          "userdata/services" = {
            type = "zfs_fs";
            mountpoint = "/DATA/services";
            options."com.sun:auto-snapshot" = "false";
          };
        };
      };
    };
  };
}
