{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./disko-config.nix

    # ./modules/podman.nix

    ../../modules/base.nix
    ../../modules/dnscrypt.nix
    ../../modules/firejail.nix
    ../../modules/sanoid.nix
    ../../modules/zram.nix
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age = {
      sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
      generateKey = false;
    };

    secrets.rootPassphrase.owner = "root";
    secrets.mkoPassphrase.owner = "root";
    # used as "cloaking_rules"
    secrets.extraHosts.owner = "dnscrypt-proxy";
  };

  # nixpkgs.currentSystem = "x86_64-linux";
  nix.settings.trusted-users = ["@wheel" "root" "mko"];

  boot.kernelParams = [
    "amd_pstate=active"
    # forbid hibernation due to zfs-on-root.
    "nohibernate"
    "amdgpu.backlight=0"
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 42;
  boot.loader.systemd-boot.netbootxyz.enable = false;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.supportedFilesystems = ["zfs"];
  boot.zfs.forceImportRoot = true;

  boot.initrd.kernelModules = ["zfs" "e1000e"];

  boot.binfmt = {
    emulatedSystems = [
      "wasm32-wasi"
      "aarch64-linux"
    ];
  };

  environment.etc = {
    "greetd/environments".text = ''
      sway
    '';
    "walls/2020-August-11-Churning-Clouds-on-Jupiter.jpg".source = ./walls/2020-August-11-Churning-Clouds-on-Jupiter.jpg;
    "walls/PIA06254.jpg".source = ./walls/PIA06254.jpg;
    "walls/PIA20522enceladus.jpg".source = ./walls/PIA20522enceladus.jpg;
  };

  sound = {
    enable = true;
    mediaKeys = {enable = true;};
  };
  hardware.pulseaudio.enable = false;

  fonts = {
    packages = with pkgs; [
      #    font-awesome
      #    google-fonts
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      fira-code
      cascadia-code
      (nerdfonts.override {
        fonts = ["FiraCode" "JetBrainsMono" "CascadiaCode"];
      })
    ];

    enableDefaultPackages = true;
    fontDir.enable = true;

    fontconfig = {
      enable = true;
      defaultFonts = {
        monospace = ["FiraCode Nerd Font"];
        sansSerif = ["Noto Sans"];
        serif = ["Noto Serif"];
        emoji = ["Noto Color Emoji"];
      };
    };
  };

  environment = {
    variables = {
      EDITOR = "vim";
      VISUAL = "vim";
      MOZ_ENABLE_WAYLAND = "1";
      NIXOS_OZONE_WL = "1";
      NIXPKGS_ALLOW_UNFREE = "0";
      TERMINAL = "kitty";
      WLR_NO_HARDWARE_CURSORS = "1";
      XCURSOR_SIZE = "16";
      XDG_CURRENT_DESKTOP = "sway";
      XDG_SESSION_TYPE = "wayland";
      _JAVA_AWT_WM_NONREPARENTING = "1";
    };
    systemPackages = with pkgs; [
      home-manager
      openssl
      libinput
      dmidecode
      pamixer
      git
      wol
      vim
      wget
      curl
      kitty
      brave
      go_1_21
      cargo
      chainsaw
      topgrade
      vulnix
      keepassxc
      libreoffice
    ];
  };

  networking = {
    # hostId = pkgs.lib.mkForce "00000000";
    hostId = "deadb33f";
    hostName = "t14";

    nftables.enable = true;

    networkmanager.enable = true;
    # interfaces.enp0s25.wakeOnLan.enable = true;

    firewall = {
      allowPing = true;
    };

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  };

  users.users = {
    root = {
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBtG6NCgdLHX4ztpfvYNRaslKWZcl6KdTc1DehVH4kAL"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaXmXbNegxiXLldy/sMYX8kCsghY1SGqn2FZ5Jk7QJw"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBZbkw9vjCfbMPEH7ZAFq20XE9oIJ4w/HRIMu2ivNcej caelum's nixbldr key"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGKzPC0ZK4zrOEBUdu1KNThEleVb1T5Pl3+n3KB3o0b8 surtur's nixbldr key"
      ];
      # hashedPasswordFile = config.sops.secrets.rootPassphrase.path;
      hashedPassword = "$y$j9T$yNhN6CYvKBWz/HnLv2gp//$0fFgtV4xzBijxWxUg1oTH74GoekdMK6UZUQWby5fZi4";
      subUidRanges = [
        {
          count = 65535;
          startUid = 65536 * 28; # 1835008, docker
        }
      ];
    };

    mko = {
      isNormalUser = true;
      createHome = true;
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBtG6NCgdLHX4ztpfvYNRaslKWZcl6KdTc1DehVH4kAL"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaXmXbNegxiXLldy/sMYX8kCsghY1SGqn2FZ5Jk7QJw"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBZbkw9vjCfbMPEH7ZAFq20XE9oIJ4w/HRIMu2ivNcej caelum's nixbldr key"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGKzPC0ZK4zrOEBUdu1KNThEleVb1T5Pl3+n3KB3o0b8 surtur's nixbldr key"
      ];
      hashedPasswordFile = config.sops.secrets.mkoPassphrase.path;
      extraGroups = [
        "wheel"
        "networkmanager"
        "audio"
        "camera"
        "kvm"
        "lp"
        "scanner"
        "video"
        "console"
      ];
      subUidRanges = [
        {
          count = 65535;
          startUid = 65536 * 28; # 1835008, docker
        }
      ];
    };
  };
  users.users.mko.group = "mko";
  users.groups.mko = {};

  users.groups.wheel.members = ["mko"];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  security = {
    doas = {
      enable = false;
      extraRules = [
        {
          users = ["mko"];
          keepEnv = true; # Optional, retains environment variables while running commands
          persist = true; # Optional, only require password verification a single time
        }
      ];
    };
    sudo = {
      enable = true;
      extraRules = [
        {
          commands = [
            {
              command = "${pkgs.systemd}/bin/systemctl suspend";
              options = ["NOPASSWD"];
            }
            {
              command = "${pkgs.systemd}/bin/reboot";
              options = ["NOPASSWD"];
            }
            {
              command = "${pkgs.systemd}/bin/poweroff";
              options = ["NOPASSWD"];
            }
          ];
          groups = ["wheel"];
        }
      ];
    };
    pam.services.swaylock = {};
    polkit.enable = true;
  };

  services = {
    atd.enable = true;

    udev.extraRules = ''
      # wol
      ACTION=="add", SUBSYSTEM=="net", NAME=="en*", RUN+="${pkgs.ethtool}/bin/ethtool -s $name wol g"
    '';

    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };

    blueman.enable = true;

    dbus.enable = true;

    dnscrypt-proxy2.settings.cloaking_rules = config.sops.secrets.extraHosts.path;

    greetd = {
      enable = true;
      settings = {
        default_session.command = ''
          ${pkgs.greetd.tuigreet}/bin/tuigreet \
            --time \
            --asterisks \
            --user-menu \
            --cmd sway
        '';
      };
    };

    power-profiles-daemon.enable = true;
    #tlp.enable =
    #  lib.mkDefault ((lib.versionOlder (lib.versions.majorMinor lib.version) "23.11")
    #    || !config.services.power-profiles-daemon.enable);
    #auto-cpufreq.enable = true;
    #auto-cpufreq.settings = {
    #  battery = {
    #    governor = "powersave";
    #    turbo = "never";
    #  };
    #  charger = {
    #    governor = "schedutil";
    #    turbo = "auto";
    #  };
    #};

    prometheus = {
      # WIP.
      enable = true;
      # openFirewall = true;
      port = 9090;
      exporters = {
        node = {
          enable = true;
          enabledCollectors = [
            "logind"
            "systemd"
          ];
          port = 9100;
        };
      };

      scrapeConfigs = [
        {
          job_name = "node";
          static_configs = [
            {
              targets = [
                "${config.networking.hostName}.local:${toString config.services.prometheus.exporters.node.port}"
              ];
            }
          ];
        }
      ];
    };

    sanoid = {
      enable = true;
      interval = "*:0/5";
      datasets = {
        "zroot/userdata" = {
          useTemplate = ["frequent"];
          recursive = "zfs";
        };
        "zroot/system/nixos" = {
          useTemplate = ["production"];
          recursive = "zfs";
        };
        "zroot/local/nix" = {
          useTemplate = ["production"];
        };
        "zroot/local/xper" = {
          useTemplate = ["production"];
        };
      };
    };

    syncthing = {
      enable = true;
      openDefaultPorts = true;
      dataDir = "/home/mko/sync";
      configDir = "/home/mko/.config/syncthing";
      user = "mko";
      group = "mko";
      guiAddress = "127.0.0.1:8384";
    };

    # TS is enabled in the imported module, this is additional config.
    tailscale = {
      useRoutingFeatures = "both";
      # accept-routes = true;
    };

    zfs = {
      autoScrub = {
        enable = true;
        interval = "weekly";
      };
      trim.enable = true;
    };
  };

  virtualisation.libvirtd.enable = true;
  virtualisation.docker.enable = true;
  virtualisation.docker.daemon.settings = {userns-remap = "dockremap:dockremap";};
  ## rootless.
  #virtualisation.docker.rootless = {
  #  enable = true;
  #  setSocketVariable = true;
  #};
  virtualisation.docker.storageDriver = "zfs";
  users.users.dockremap = {
    isNormalUser = false;
    isSystemUser = true;
    createHome = false;
    shell = pkgs.zsh;
    extraGroups = [
      "docker"
    ];
    subUidRanges = [
      {
        count = 65535;
        startUid = 65536 * 30;
      }
    ];
    subGidRanges = [
      {
        count = 65535;
        startGid = 65536 * 30;
      }
    ];
  };
  users.users.dockremap.group = "dockremap";
  users.groups.dockremap = {};

  hardware = {
    bluetooth = {
      enable = true;
      # HSP & HFP daemon (apparently needs to be false now because of wire plumber)
      hsphfpd.enable = false;
      settings = {General = {Enable = "Source,Sink,Media,Socket";};};
    };
    opengl = {
      enable = true;
      extraPackages = with pkgs; [
        vaapiVdpau
        libvdpau-va-gl
      ];
    };
  };

  xdg = {
    portal = {
      enable = true;
      wlr.enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr
        xdg-desktop-portal-gtk
      ];
    };
  };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # Does not work with flakes - yet™.
  system.copySystemConfiguration = false;
}
