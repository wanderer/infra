{
  config,
  pkgs,
  lib,
  ...
}: let
  # bash script to let dbus know about important env variables and
  # propagate them to relevent services run at the end of sway config
  # see
  # https://github.com/emersion/xdg-desktop-portal-wlr/wiki/"It-doesn't-work"-Troubleshooting-Checklist
  # note: this is pretty much the same as  /etc/sway/config.d/nixos.conf but also restarts
  # some user services to make sure they have the correct environment variables
  dbus-sway-environment = pkgs.writeTextFile {
    name = "dbus-sway-environment";
    destination = "/bin/dbus-sway-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
      systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  # currently, there is some friction between sway and gtk:
  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
  # the suggested way to set gtk settings is with gsettings
  # for gsettings to work, we need to tell it where the schemas are
  # using the XDG_DATA_DIR environment variable
  # run at the end of sway config
  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Dracula'
    '';
  };
in {
  home.packages = with pkgs; [
    dbus-sway-environment
    configure-gtk

    kitty
    alacritty # gpu accelerated terminal

    wayland
    xdg-utils # for opening default programs when clicking links
    glib # gsettings
    dracula-theme # gtk theme
    gnome3.adwaita-icon-theme # default gnome cursors
    swaylock-effects
    swayidle
    grim # screenshot functionality
    slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    bemenu # wayland clone of dmenu
    mako # notification system developed by swaywm maintainer
    wdisplays # tool to configure displays manually
    kanshi
    wofi
    farge
    j4-dmenu-desktop
    swayr
    avizo
    light
    swayfx
    swaybg
  ];

  ## enable sway window manager
  #programs.sway = {
  #  enable = true;
  #  wrapperFeatures.gtk = true;
  #};
  wayland.windowManager.sway = {
    enable = true;
    package = pkgs.swayfx;
    systemd.enable = true;
    config = rec {
      modifier = "Mod4"; # super key
      # Use kitty as default terminal
      terminal = "kitty";
      startup = [
        # Launch Firefox on start
        # {command = "firefox";}
        {command = "kitty";}
      ];
      bars = [];
    };
    extraConfig = ''
      set {
      	$mod Mod4
        # $mod4 Mod4
      	$left h
      	$down j
      	$up k
      	$right l
        # $lock exec swaylock -efFc "#000000"
      	$lock exec swaylock -efF

        $bg "/etc/walls/2020-August-11-Churning-Clouds-on-Jupiter.jpg"
        # $bg "/etc/walls/PIA06254.jpg"
        # $bg "/etc/walls/PIA20522enceladus.jpg"

        $gtk_theme "Dracula"
        ###  $gtk_theme "Yaru-purple-dark"
        ###  $gtk_font_name "CascadiaCodePL ExtraLight"
        ###  $gtk_font_size "10.4"
        ###  $icon_theme "Flat-Remix-Blue-Dark"
        ###  # $icon_theme "Yaru-blue-dark"
        ###  # $gtk_cursor_theme "Yaru"
        ###  $gtk_cursor_theme "Adwaita"
        ###  $cursor_size 16
        ###  $theme "Yaru-purple-dark"

      	$laptop_screen "eDP-1"
      	$ext_screen "Dell Inc. DELL U2412M Y1H5T27N1EVL"
      	$pri $laptop_screen
      	$sec $ext_screen

      	$term KITTY_ENABLE_WAYLAND=1 kitty -1 # qterminal alacritty
        $albert QT_QPA_PLATFORM=xcb albert show
      	$menu wofi --show drun --allow-markup swaymsg exec --
      	$othermenu XDG_DATA_DIRS=/usr/share/:/usr/local/share/:/var/lib/flatpak/exports/share/:$HOME/.local/share/flatpak/exports/share/ j4-dmenu-desktop --dmenu="bemenu -n -p \"applications\" -l 30 --fn 'FiraCode Retina 17'" --term="kitty" --no-generic --display-binary
      	$nutermmenu kitty --class 'launcher' bash -c 'compgen -c | sort -u | fzf | xargs -r kitty'
      	$bemenu PATH=$/.cargo/bin:$HOME/go/bin:$PATH XDG_DATA_DIRS=/usr/share/:/usr/local/share/:/var/lib/flatpak/exports/share/:$HOME/.local/share/flatpak/exports/share/ bemenu-run --fork -l 5 -n -p "▶" --fn 'FiraCode Retina 17' | xargs swaymsg exec
      	$gtk_font_name "CascadiaCodePL ExtraLight"
      	$gtk_font_size "10.4"
      }

      output "*" bg $bg fill

      default_border pixel 0
      default_floating_border normal
      # hide_edge_borders --i3 both
      # smart_borders on

      ### depends on swayfx
      # blur enable
      # blur_xray enable
      # blur_passes 2
      # blur_radius 10
      # corner_radius 4
      # default_dim_inactive 0.1
      # shadows enable

      # works also for non-floating windows
      # normal - drag floating windows by holding down $mod and left mouse button.
      # resize them with right mouse button + $mod.
      # inverse for $mod+left click to resize, $mod+right click to drag
      # floating_modifier $mod normal

      # clipboard sync
      # exec wl-paste -pw wl-copy

      # Only enable outer gaps when there is exactly one container
      #smart_gaps inverse_outer
      # Only enable gaps on a workspace when there is at least one container
      #smart_gaps on
      smart_gaps inverse_outer

      # Activate smart borders (always)
      #smart_borders on
      # Activate smart borders (only when there are effectively no gaps)
      #smart_borders no_gaps

      # Hide edge borders only if there is one window with no gaps
      #hide_edge_borders smart_no_gaps

      gaps outer 8
      # gaps outer -3
      gaps inner 5

      font pango:FiraCode Retina weight=700 14px

      # seat seat0 xcursor_theme $gtk_cursor_theme $cursor_size
      exec_always --no-startup-id
      {
        #swaymsg output $pri bg $bg fill #000000
      	#swaymsg output $sec bg $bg fill #000000
      	gsettings set org.gnome.desktop.interface font-name '$gtk_font_name $gtk_font_size'
      	gsettings set org.gnome.desktop.wm.preferences button-layout 'icon:close'
      	gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
      	gsettings set org.gtk.Settings.FileChooser window-size '(1100,700)'

        # pgrep kanshi && pkill kanshi
      	pgrep swayrd && pkill swayrd
      	pgrep swayidle && pkill swayidle
        # pgrep nm-applet && pkill nm-applet
        pgrep autotiling && pkill autotiling

        # kanshi
      	env RUST_BACKTRACE=1 swayrd > /tmp/swayrd.log 2>&1
      		# timeout 545 'pgrep -u $(id -u) swaylock || notify-send -t 20000 -r 344334 -u critical "Idle timeout" "Screen is locking soon"' \

      	swayidle -w \
      		timeout 545 'pgrep swaylock || notify-send -t 18000 -u normal "Idle timeout" "Screen is locking soon"' \
      		timeout 600 "$lock" \
      		timeout 15 'pgrep swaylock && swaymsg "output * power off"' \
      		resume 'swaymsg "output * power on"' \
      		before-sleep "pgrep swaylock || $lock"

        # TODO(me):
        # 	https://github.com/slgobinath/SafeEyes
        # nm-applet --indicator
        autotiling
        # lxpolkit
      }

      # exec nm-applet --indicator
      # exec env RUST_BACKTRACE=1 swayrd > /tmp/swayrd.log 2>&1
      # exec autotiling

      # hide mouse after some seconds of inactivity
      seat * hide_cursor 13000

      bindgesture swipe:right workspace prev
      bindgesture swipe:left workspace next

      # Allow container movements by pinching them
      bindgesture pinch:inward+up move up
      bindgesture pinch:inward+down move down
      bindgesture pinch:inward+left move left
      bindgesture pinch:inward+right move right

      bindswitch --reload --locked lid:on exec --no-startup-id $(swaymsg output $laptop_screen disable && kanshictl reload)
      bindswitch --reload --locked lid:off exec --no-startup-id $(swaymsg output $laptop_screen enable && kanshictl reload)

      exec --no-startup-id swaymsg focus output $pri

      #--- Window Decorations
      set
      {
      	$nofocus	#202020
      	$focus		#202020
      	$titletxt	#FFFFE8
      	$titlepad	10 center #000000
      	$gaps_change	20
      }
      #--- Notifications
      set
      {
      	$n_anchor	top-center
      	$n_layer	overlay
      	$n_dim		600
      	$n_margin	20
      	$n_padding	10
      	$n_border	3
      	$n_bg		#202020
      	$n_fg		#FFFFE8
      }

      bindsym
      {
        ctrl+alt+l exec --no-startup-id $lock
        $mod+grave exec $lock

        --no-warn $mod+Return exec $term
        --no-warn $mod+t exec $term
      	$mod+ctrl+Shift+t exec qterminal
        --no-warn $mod+Shift+q kill
      	alt+Shift+F1 exec $menu
      	alt+F1 exec $albert
      	alt+F2 exec $bemenu
      	alt+F3 exec $othermenu
      	alt+Shift+F2 exec $nutermmenu
      	$mod+ctrl+k exec QT_AUTO_SCREEN_SCALE_FACTOR=0 QT_QPA_PLATFORMTHEME=wayland keepassxc
      }

      include ${./sway-inputs.sway}

      focus_follows_mouse yes
      focus_wrapping false
      popup_during_fullscreen smart


      for_window [app_id="^launcher$"] floating enable, border none, resize set width 25 ppt height 100 ppt, move position 0 px 0 px

      #--- Floating Windows ------------------------------------------------------
      for_window [window_role="pop-up"] floating enable
      for_window [window_role="pop-up"]	floating enable; border normal $border
      for_window [class="pop-up"]		floating enable; border normal $border
      for_window [window_role="bubble"] floating enable
      for_window [window_role="task_dialog"] floating enable
      for_window [window_role="task_dialog"]	floating enable; border normal $border
      for_window [window_role="Preferences"] floating enable
      for_window [window_role = "dialog"] floating enable
      for_window [window_type="dialog"] floating enable
      for_window [window_role = "task_dialog"] floating enable
      for_window [window_type="menu"] floating enable
      for_window [app_id = "floating"] floating enable
      for_window [app_id = "floating_update"] floating enable, resize set width 1000px height 600px
      for_window [class = "(?i)pinentry"] floating enable
      for_window [window_role="About"] floating enable
      for_window [title = "About Mozilla Firefox"] floating enable
      for_window [instance="Farge"] floating enable
      for_window [title = ".*kdbx - KeePass"] floating enable, resize set 1276px 814pxofor_window [app_id = "virt-manager"] floating enable
      for_window [app_id = "xfce-polkit"] floating enable

      for_window [app_id = "keepassxc"] floating enable, resize set 1276px 814px

      for_window [title = "^Open File$"] floating enable
      for_window [app_id = "avizo-service"] border pixel 0, sticky toggle
      no_focus [app_id="avizo-service"]
      for_window [window_role = "GtkFileChooserDialog"] resize set 590 340
      for_window [window_role = "GtkFiileChooserDialog"] move position center

      for_window [title="Save File"] floating enable
      for_window [app_id="wdisplays"] floating enable
      for_window [app_id="floating_shell_portrait"] floating enable, border pixel 1, sticky enable, resize set width 30 ppt height 40 ppt
      for_window [app_id="floating_shell"] floating enable, border pixel 1, sticky enable
      for_window [app_id = "qt5ct"] floating enable
      for_window [app_id = "gnome-tweaks"] floating enable
      for_window [class = "Bluetooth-sendto" instance = "bluetooth-sendto"] floating enable
      for_window [window_role = "Preferences"] floating enable
      for_window [app_id="(?i)(?:blueman-manager|azote|gnome-disks|nwg-shell-config)"] floating enable
      for_window [app_id="(?i)(?:pavucontrol|nm-connection-editor|gsimplecal|galculator)"] floating enable
      for_window [class="MEGAsync"]		floating enable; border pixel 0
      for_window [title=".*JetBrains.*"]	floating enable; border pixel 1
      for_window [class="matplotlib"]		floating enable; border normal $border
      for_window [class="dosbox"]		floating enable; border normal $border
      for_window [app_id = "nm-openconnect-auth-dialog"] floating enable
      for_window [app_id="nm-connection-editor"]
      {
      	floating enable
      	resize set width  882px
      	resize set height 600px
      }
      #for_window [app_id="pavucontrol"]
      #{
      #	floating enable
      #	resize set width  882px
      #	resize set height 600px
      #}
      # for_window [app_id="ulauncher"]		border none

      # Scratchpad terminal
      for_window [title="(?i)$criteria"] floating enable, move absolute position 1500px 300px, resize set 1200px 700px, move to scratchpad, mark $criteria

      # Waybar Tooltips don't steal focus
      no_focus [app_id="waybar"]
      for_window [app_id="waybar" floating] {
          move position cursor
          move down 60px # adjust if some menus still don't fit
      }

      #--- Applications with CSD -------------------------------------------------
      for_window [app_id="glade"]		border pixel 2
      for_window [app_id="baobab"]		border pixel 2
      for_window [title=".*eston.*"]		border pixel 0
      for_window [app_id="wdisplays"]		border pixel 0
      for_window [app_id="gnome-.*"]		border pixel 1
      for_window [app_id="qterminal"]		border pixel 2
      for_window [app_id="xterm"]		border pixel 2
      for_window [title=".*Brave.*"]		border pixel 1 inhibit_idle fullscreen
      for_window [title=".*Firefox.*"]	border pixel 2 inhibit_idle fullscreen
      for_window [title="Firefox.*"]	border pixel 2 inhibit_idle fullscreen
      for_window [app_id="firefox"]	border pixel 2 inhibit_idle fullscreen
      # for_window [title=".*Firefox.*"]	border none inhibit_idle fullscreen
      # for_window [app_id="firefox"]	border none inhibit_idle fullscreen
      for_window [class="Firefox"]	border pixel 2 inhibit_idle fullscreen
      for_window [class="swayfox"]	border pixel 2 inhibit_idle fullscreen
      for_window [class="vlc"]		border pixel 2 inhibit_idle visible fullscreen
      for_window [app_id="vlc"]		border pixel 2 inhibit_idle visible fullscreen
      for_window [title=".*VLC*."]		border pixel 2 inhibit_idle visible fullscreen
      for_window [title="VLC*."]		border pixel 2 inhibit_idle visible fullscreen

      # prevent screen locking if any app is fullscreen.
      # matching per-class is tedious.
      for_window [class=".*"] inhibit_idle fullscreen
      for_window [app_id=".*"] inhibit_idle fullscreen

      for_window [app_id="org.gnome.Nautilus"]		border pixel 1
      for_window [app_id="dolphin"]		border pixel 1
      # for_window [app_id="kitty"]		border pixel 1
      for_window [app_id="kitty"]		border pixel 2


      # for_window [title="^video0 - mpv$"] floating enable, sticky enable, resize set height 260 px, move absolute position 0 px 820 px, border none
      for_window [title="^video0 - mpv$"] floating enable, sticky enable, resize set height 130 px, border none
      for_window [title="Firefox Developer Edition — Sharing Indicator$"] floating enable, resize set height 16 px, border none
      for_window [title="Picture-in-Picture"] floating enable

      #--- Xtk Applications ------------------------------------------------------
      for_window [title="xclock"]		floating enable
      for_window [title="Calculator"]		floating enable

      assign [app_id="telegram"] C
      assign [class="Signal"] C
      assign [title="Ferdi"] C
      assign [title="Discord"] C
      assign [class="Discord"] C
      assign [class="vlc"] M


      bindsym
      {
        Alt+Shift+e exec BEMOJI_PICKER_CMD="bemenu -n -l 10  -p 🔎 --fn 'Fira Code Retina 20" bemoji -c -n --private

        # as per https://github.com/japhir/ArchConfigs/blob/master/swayconfig
        # add the webcam screen for video recording
        $mod+ctrl+Shift+a exec mpv --no-osc --demuxer-lavf-format=video4linux2 --demuxer-lavf-o-set=input_format=mjpeg av://v4l2:/dev/video0 || notify-send "failed to start webcam"

      	# screenshot of all outputs
      	print exec grim $(xdg-user-dir PICTURES)/screenshots/grim-$(date +'%Y-%m-%dT%H-%M-%S').png
      	# screenshot of an area
      	Shift+print exec grim -g "$(slurp)" - | wl-copy
      	# screenshot of a window
        ## Control+print exec winprint.sh
      	# colour picker / eyedropper
        $mod+Shift+Alt+f exec farge --notify
      	Control+Shift+print exec grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt: | wl-copy
        $mod+tab exec swayr switch-workspace-or-window
      	$mod+c exec gnome-calculator
      	# -w: Always open a new window for browsing specified URIs.
      	$mod+n exec nautilus -w
      	$mod+Shift+b exec brave --incognito $chromium_flags
      	$mod+Shift+w exec MOZ_USE_XINPUT2=1 MOZ_ENABLE_WAYLAND=1 ~/Downloads/firefox-dev/firefox --class=swayfox --private-window
      	$mod+Ctrl+Shift+w exec MOZ_USE_XINPUT2=1 MOZ_ENABLE_WAYLAND=1 systemctl --user start ff_nn
      	$mod+Shift+g exec MOZ_USE_XINPUT2=1 MOZ_ENABLE_WAYLAND=1 jailfirefox --class=swayfox --private-window
      	$mod+Shift+a exec jailchromium --incognito
      }


      #
      # Borders
      #
      bindsym --to-code
      {
      	$mod+ctrl+semicolon border toggle
      }

      #
      # Gaps
      #
      bindsym --to-code
      {
      	# $mod+ctrl+minus gaps inner 2px
      	# $mod+ctrl+equal gaps inner 2px
      	$mod+z       gaps outer current plus 5
      	$mod+Shift+z gaps outer current minus 5
      	$mod+x       gaps inner current plus 5
      	$mod+Shift+x gaps inner current minus 5
      }

      # Notifications
      bindsym Control+Space exec makoctl dismiss
      bindsym Control+Shift+Space exec makoctl dismiss --all
      bindsym Control+Alt+Space exec makoctl set-mode do-not-disturb
      bindsym Control+Alt+Shift+Space exec makoctl set-mode default

      #
      # Moving around:
      #
      bindsym --no-warn
      {
      	# Move your focus around
        $mod+$left focus left
      	$mod+$down focus down
      	$mod+$up focus up
      	$mod+$right focus right
      	# Or use $mod+[up|down|left|right]
        $mod+Left focus left
      	$mod+Down focus down
      	$mod+Up focus up
      	$mod+Right focus right

      	# Move the focused window with the same, but add Shift
        $mod+Shift+$left move left
      	$mod+Shift+$down move down
      	$mod+Shift+$up move up
      	$mod+Shift+$right move right
      	# Ditto, with arrow keys
        $mod+Shift+Left move left
      	$mod+Shift+Down move down
      	$mod+Shift+Up move up
      	$mod+Shift+Right move right

        $mod+Shift+c move position center
      }

      bindsym --to-code
      {
      	$mod+Shift+comma  workspace prev_on_output
      	$mod+Shift+period workspace next_on_output
      	$mod+Shift+tab    workspace back_and_forth
      	$mod+comma focus output left
      	$mod+period focus output right
      	$mod+F2 workspace C
      	$mod+F3 workspace M
      }

      # Move focused container to workspace
      bindsym --to-code
      {
      	$mod+Shift+F2 move container to workspace C
      	$mod+Shift+F3 move container to workspace M
      }

      #
      # Workspaces:
      #
      # Switch to workspace
      bindsym --no-warn
      {
        $mod+1 workspace number 1
        $mod+2 workspace number 2
        $mod+3 workspace number 3
        $mod+4 workspace number 4
        $mod+5 workspace number 5
        $mod+6 workspace number 6
        $mod+7 workspace number 7
        $mod+8 workspace number 8
        $mod+9 workspace number 9
        $mod+0 workspace number 0
      }

      # Move focused container to workspace
      bindsym --no-warn
      {
        $mod+Shift+1 move container to workspace number 1
        $mod+Shift+2 move container to workspace number 2
        $mod+Shift+3 move container to workspace number 3
        $mod+Shift+4 move container to workspace number 4
        $mod+Shift+5 move container to workspace number 5
        $mod+Shift+6 move container to workspace number 6
        $mod+Shift+7 move container to workspace number 7
        $mod+Shift+8 move container to workspace number 8
        $mod+Shift+9 move container to workspace number 9
        $mod+Shift+0 move container to workspace number 0
      }

      #
      # Workspace-monitor assignments
      #
      workspace 0 output $pri
      workspace 1 output $pri
      workspace 2 output $pri
      workspace 3 output $pri
      workspace 4 output $pri
      workspace 5 output $pri
      workspace 6 output $pri
      workspace 7 output $pri
      workspace 8 output $pri
      workspace 9 output $pri
      workspace C output $pri
      workspace M output $pri
      workspace 10 output $sec
      workspace 11 output $sec
      workspace 12 output $sec
      workspace 13 output $sec
      workspace 14 output $sec
      workspace 15 output $sec
      workspace 16 output $sec
      workspace 17 output $sec
      workspace 18 output $sec
      workspace 19 output $sec
      workspace 20 output $ter
      workspace 21 output $ter
      workspace 22 output $ter
      workspace 23 output $ter
      workspace 24 output $ter
      workspace 25 output $ter
      workspace 26 output $ter
      workspace 27 output $ter
      workspace 28 output $ter
      workspace 29 output $ter

      #
      # Layout stuff:
      #
      bindsym --no-warn
      {
      	# You can "split" the current object of your focus with
      	# $mod+b or $mod+v, for horizontal and vertical splits
      	# respectively.
        $mod+b splith
        $mod+v splitv

      	# Switch the current container between different layout styles
        $mod+s layout stacking
        $mod+w layout tabbed
        $mod+e layout toggle split

      	# Make the current focus fullscreen
        $mod+f fullscreen

      	# for in-container fullscreen chromium
      	# ref: https://www.reddit.com/r/i3wm/comments/egvj3k/chrome_tabs_not_visible_in_full_screen_mode/fdeps34/
      	$mod+Shift+f split v; focus parent; fullscreen toggle; focus child

      	# Toggle the current focus between tiling and floating mode
      	#bindsym $mod+Shift+space floating toggle
      	$mod+Shift+d floating toggle

      	# Swap focus between the tiling area and the floating area
      	#bindsym $mod+space focus mode_toggle
      	$mod+d focus mode_toggle

      	# Move focus to the parent container
      	$mod+a focus parent

      	# focus the child container
      	$mod+alt+a focus child
      }

      #
      # Scratchpad:
      #
      	# Sway has a "scratchpad", which is a bag of holding for windows.
      	# You can send windows there and get them back later.

      bindsym --to-code
      {
      	# Move the currently focused window to the scratchpad
      	$mod+Shift+minus move scratchpad

      	# Show the next scratchpad window or hide the focused scratchpad window.
      	# If there are multiple scratchpad windows, this command cycles through them.
      	$mod+minus scratchpad show
      }

      #
      # Resizing containers:
      #
      mode "resize"
      {
      	# left will shrink the containers width
      	# right will grow the containers width
      	# up will shrink the containers height
      	# down will grow the containers height
      	bindsym --no-warn
      	{
      	$left resize shrink width 10px
      	$down resize grow height 10px
      	$up resize shrink height 10px
      	$right resize grow width 10px

      	# Ditto, with arrow keys
      	Left resize shrink width 10px
      	Down resize grow height 10px
      	Up resize shrink height 10px
      	Right resize grow width 10px

      	# Return to default mode
      	Return mode "default"
      	Escape mode "default"
      	$mod+r mode "default"
      	}
      }
      bindsym --no-warn $mod+r mode "resize"
      #bindsym --no-warn $mod+Shift+r reload; exec kanshictl reload
      bindsym --no-warn $mod+Shift+r reload

      # kudos to github.com/rbnis for the following menu-like section inspiration
      # https://github.com/rbnis/dotfiles/blob/dfd6f956f6d00a1012a3a167d947773095dac7fd/.config/sway/config#L228-L235
      set $mode_sys System: (l) lock, (e) logout, (s) suspend, (r) reboot, (S) shutdown, (⏎,␛) return
      mode "$mode_sys" {
      	bindsym {
      		l exec --no-startup-id $lock, mode "default"
      		e exit
      		s exec --no-startup-id systemctl suspend -i, mode "default"
      		r exec --no-startup-id systemctl reboot, mode "default"
      		Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
      		# return to default mode
      		Return mode "default"
      		Escape mode "default"
      		$mod+Shift+e mode "default"
      	}
      }
      bindsym --no-warn $mod+Shift+e mode "$mode_sys"

      bindsym XF86Sleep exec "systemctl suspend -i"

      #
      # media keys
      #
      bindsym
      {
        # depends on avizo-service
      	--locked XF86AudioRaiseVolume exec volumectl -u up
      	--locked XF86AudioLowerVolume exec volumectl -u down
      	--locked XF86AudioMute exec volumectl toggle-mute
      	--locked XF86AudioMicMute exec volumectl -m toggle-mute
      	--locked XF86MonBrightnessUp exec lightctl up
      	--locked XF86MonBrightnessDown exec lightctl down

      	--locked $mod+Ctrl+p exec playerctl play-pause
      	--locked $mod+Ctrl+n exec playerctl next
      	--locked $mod+Ctrl+Shift+n exec playerctl previous
      	--locked XF86AudioPlay exec playerctl play-pause
      	--locked XF86AudioNext exec playerctl next
      	--locked XF86AudioPrev exec playerctl previous

      	# --locked XF86AudioMute exec amixer set Master toggle
      	# --locked XF86AudioMicMute exec amixer set Capture toggle;exec notify-send -t 3600 'mic mute toggle'
      	# --locked XF86AudioMicMute exec ydotool key 248:1 248:0;exec notify-send -t 3600 'mic mute toggle'
      }

      include ${./dracula.sway}
    '';
  };
}
