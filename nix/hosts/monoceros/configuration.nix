{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./disko-config.nix

    # ./modules/caddy.nix
    ../../modules/base.nix
    ../../modules/dnscrypt.nix
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age = {
      # keyFile = "/root/.age/monoceros";
      sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
      generateKey = false;
    };

    secrets.rootPassphrase.owner = "root";
  };

  # nixpkgs.currentSystem = "x86_64-linux";
  nix.settings.trusted-users = ["@wheel" "root"];

  # forbid hibernation due to zfs-on-root.
  boot.kernelParams = ["nohibernate"];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 42;
  boot.loader.systemd-boot.editor = false;
  boot.loader.systemd-boot.netbootxyz.enable = false;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.supportedFilesystems = ["zfs"];
  boot.zfs.forceImportRoot = true;

  boot.initrd.secrets = {
    # "/root/initrd-ssh-key" = "/root/initrd-ssh-key";
    "/root/initrd-ssh-host-ed25519_key" = "/root/initrd-ssh-host-ed25519_key";
  };
  boot.initrd.kernelModules = ["zfs" "e1000e"];
  boot.initrd.network = {
    # This will use udhcp to get an ip address.
    # Make sure you have added the kernel module for your network driver to `boot.initrd.availableKernelModules`,
    # so your initrd can load it!
    # Static ip addresses might be configured using the ip argument in kernel command line:
    # https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt
    enable = true;
    ssh = {
      enable = true;
      # To prevent ssh clients from freaking out because a different host key is used,
      # a different port for ssh is useful (assuming the same host has also a regular sshd running)
      postCommands = ''
        echo "zfs load-key zroot/nixos && killall zfs" >> /root/.profile
      '';

      port = 2222;
      # hostKeys paths must be unquoted strings, otherwise you'll run into issues with boot.initrd.secrets
      # the keys are copied to initrd from the path specified; multiple keys can be set
      # you can generate any number of host keys using
      # `ssh-keygen -t ed25519 -N "" -f /path/to/ssh_host_ed25519_key`
      hostKeys = [/root/initrd-ssh_host_ed25519_key];
      ignoreEmptyHostKeys = true;
      authorizedKeys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODmLwtQj6ylgdTPo1/H5jW7jsLzwaCTGdIsTQAdc896"];
    };
  };
  # boot.initrd.systemd.contents

  boot.binfmt = {
    emulatedSystems = [
      "wasm32-wasi"
      "aarch64-linux"
    ];
  };

  networking = {
    # hostId = pkgs.lib.mkForce "00000000";
    hostId = "deadb33f";
    hostName = "monoceros";

    usePredictableInterfaceNames = false;
    interfaces.eth0 = {
      ipv6.addresses = [ {
        address = "2a02:c206:2153:0314:0000:0000:0000:0001";
        prefixLength = 64;
      }];
      ipv4.addresses = [ {
        address = "158.220.120.164";
        prefixLength = 20;
      } ];
    };

    nftables.enable = true;

    networkmanager.enable = false;

    firewall = {
      allowPing = true;
    };

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  };

  users.users.root = {
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBtG6NCgdLHX4ztpfvYNRaslKWZcl6KdTc1DehVH4kAL"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaXmXbNegxiXLldy/sMYX8kCsghY1SGqn2FZ5Jk7QJw"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBZbkw9vjCfbMPEH7ZAFq20XE9oIJ4w/HRIMu2ivNcej caelum's nixbldr key"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGKzPC0ZK4zrOEBUdu1KNThEleVb1T5Pl3+n3KB3o0b8 surtur's nixbldr key"
    ];
    hashedPasswordFile = config.sops.secrets.rootPassphrase.path;
    subUidRanges = [
      {
        count = 65536;
        startUid = 65536 * 28; # 1835008, docker
      }
    ];
  };

  services = {
    avahi.enable = lib.mkForce false;

    atd.enable = true;

    power-profiles-daemon.enable = false;
    #tlp.enable =
    #  lib.mkDefault ((lib.versionOlder (lib.versions.majorMinor lib.version) "23.11")
    #    || !config.services.power-profiles-daemon.enable);
    auto-cpufreq.enable = false;

    # TS is enabled in the imported module, this is additional config.
    tailscale = {
      useRoutingFeatures = "both";
      # accept-routes = true;
    };

    zfs = {
      autoScrub = {
        enable = true;
        interval = "weekly";
      };
      trim.enable = true;
    };
  };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # Does not work with flakes - yet™.
  system.copySystemConfiguration = false;
}
