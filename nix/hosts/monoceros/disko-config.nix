{
  config,
  disks ? ["/dev/sda"],
  lib,
  ...
}: let
  zfs-DATA = config.age.secrets.zfs-DATA;
  p = config.sops.placeholder;
in {
  sops.secrets = {
   "zfs/ROOT" = p."zfs/ROOT".path;
  };
  systemd.services.zfs-mount.requires = ["sops-nix.service"];

  disko.devices = {
    disk = {
      x = {
        type = "disk";
        device = "/dev/sda";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              type = "EF00";
              size = "700M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
          };
        };
      };
    };
    zpool = {
      zroot = {
        type = "zpool";
        mode = ""; # == single disk
        options = {
          ashift = "12";
          autotrim = "on";
        };
        rootFsOptions = {
          checksum = "sha512";
          compression = "zstd";
          "com.sun:auto-snapshot" = "false";
        };
        mountpoint = null;
        postCreateHook = "zfs snapshot zroot@blank";

        datasets = {
          "ROOT" = {
            type = "zfs_fs";
            mountpoint = null;
            options."com.sun:auto-snapshot" = "false";
          };
          "ROOT/nixos" = {
            type = "zfs_fs";
            mountpoint = "/";
            options = {
              encryption = "aes-256-gcm";
              keyformat = "passphrase";
              keylocation = "file:///root/.zfs-ROOT.key";
              "com.sun:auto-snapshot" = "true";
            };
            postCreateHook = ''
              zfs set keylocation="prompt" "zroot/$name";
            '';
          };
          nix = {
            type = "zfs_fs";
            mountpoint = "/nix";
            options = {
              encryption = "aes-256-gcm";
              keyformat = "passphrase";
              keylocation = "file:///root/.zfs-nix.key";
              "com.sun:auto-snapshot" = "true";
            };
          };
          DATA = {
            type = "zfs_fs";
            mountpoint = "none";
            options = {
              encryption = "aes-256-gcm";
              keyformat = "passphrase";
              keylocation = "file:///root/.zfs-DATA.key";
              "com.sun:auto-snapshot" = "true";
            };
            # postCreateHook = ''
            # zfs set keylocation="file://${zfs-DATA}.path" "zroot/$name";
            # '';
          };
          "DATA/var-lib" = {
            type = "zfs_fs";
            options = {
              mountpoint = "/var/lib";
              "com.sun:auto-snapshot" = "true";
            };
          };
          "DATA-no-backup" = {
            type = "zfs_fs";
            options = {
              mountpoint = "none";
              "com.sun:auto-snapshot" = "false";
            };
          };
        };
      };
    };
  };
}
