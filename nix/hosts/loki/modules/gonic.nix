{config, ...}: {
  services.gonic = {
    enable = true;
    settings = {
      music-path = "/DATA/music";
      podcast-path = "/DATA/podcasts";
      cache-path = "/var/cache/gonic";
      # db-path           ="/DATA/services/gonic/gonic.db";
      db-path = "/var/lib/gonic/gonic.db";
      jukebox-enabled = false;
      listen-addr = "127.0.0.1:4747";
      scan-interval = 60;
    };
  };
}
