{
  config,
  pkgs,
  ...
}: let
  ver = "26.0.2";
  baseDir = "/DATA/services/nextcloud";
  p = config.sops.placeholder;
  usr = "nx";
in {
  virtualisation.oci-containers.containers."nextcloud" = {
    autoStart = true;
    image = "lscr.io/linuxserver/nextcloud:${ver}";
    volumes = [
      "${baseDir}/data:/data"
      "${baseDir}/certs:/config/keys"
      "${baseDir}/config/config.php:/config/www/nextcloud/config/config.php"
    ];
    ports = ["127.0.0.1:8078:443"];
    environment = {
      # PUID = "${config.users.users.nx.uid}";
      # GUID = "${config.users.groups.nx.gid}";
      TZ = "Europe/Vienna";
    };
  };
  users.users.nx = {
    group = usr;
    home = "/etc/" + usr;
    createHome = false;
    isSystemUser = true;
    extraGroups = ["users"];
    subUidRanges = [
      {
        count = 65535;
        startUid = 65536 * 29;
      }
    ];
  };
  users.groups.nx = {};
}
