{
  config,
  pkgs,
  ...
}: {
  imports = [
    ../../../modules/authelia.nix
  ];

  age = {
    secrets.autheliaEnv.file = ./secrets/autheliaEnv.age;
    secrets.autheliaStorage.file = ./secrets/autheliaStorage.age;
    secrets.autheliaJWT.file = ./secrets/autheliaJWT.age;
    secrets.autheliaStorage.owner = "${toString config.services.authelia.instances.main.user}";
    secrets.autheliaJWT.owner = "${toString config.services.authelia.instances.main.user}";
  };

  services = {
    authelia.instances.main = {
      secrets.storageEncryptionKeyFile = config.age.secrets.autheliaStorage.path;
      secrets.jwtSecretFile = config.age.secrets.autheliaJWT.path;

      settings = {
        access_control = {
          rules = [
            {
              domain = "*.*";
              policy = "one_factor";
            }
          ];
        };

        storage.local.path = "/var/lib/authelia-main/data";
        authentication_backend.file.path = "/var/lib/authelia-main/users_database.yml";
        notifier.filesystem.filename = "/var/lib/authelia-main/notif.txt";

        # ntp.address = "ptbtime.ptb.de:123"
        ntp.disable_startup_check = true;
      };
    };
  };

  systemd.services.authelia-main.serviceConfig = {
    EnvironmentFile = config.age.secrets.autheliaEnv.path;
  };
}
