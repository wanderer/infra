{
  config,
  pkgs,
  ...
}: {
  virtualisation = {
    containers.storage.settings = {
      # configure for zfs.
      storage = {
        driver = "zfs";
        graphroot = "/var/lib/containers/storage";
        runroot = "/run/containers/storage";
      };
    };

    ## setup podman.
    podman = {
      enable = true;
      dockerCompat = true;
      defaultNetwork.settings.dns_enable = true;
    };
    oci-containers = {
      ## use podman as the default container engine.
      backend = "podman";
    };
  };
}
