{
  config,
  pkgs,
  sops-nix,
  ...
}: let
  caddyPkg = pkgs.callPackage ../../../modules/caddy-custom-package.nix {
    plugins = [
      "github.com/caddy-dns/njalla"
      "github.com/caddy-dns/desec"
      "github.com/dulli/caddy-wol"
      "github.com/ueffel/caddy-brotli"
      "github.com/greenpau/caddy-security"
    ];
    vendorSha256 = "sha256-4Yzqo8aUUivNtgV7hljzoN9VZ5J51AQgV+NrbZ8on5s=";
  };
  p = config.sops.placeholder;
  domain = p.domainName;
  svc = "caddy.service";
in {
  networking.firewall.allowedTCPPorts = [80 443];

  services = {
    caddy = {
      enable = true;
      package = caddyPkg;
      configFile = config.sops.templates.caddyPls.path;
      adapter = "caddyfile";
    };
  };

  systemd.services.caddy = {
    description = "Caddy web server";
    after = ["network-online.target" "sops-nix.service"];
    wants = ["network-online.target"];
    wantedBy = ["multi-user.target"];
    serviceConfig = {
      TimeoutStopSec = "5s";
      # LimitNOFILE = 1048576;
      # LimitNPROC = 512;
      PrivateTmp = true;
      # ProtectSystem = "full";
      AmbientCapabilities = "cap_net_bind_service";
    };
  };

  sops.secrets = {
    "caddy/njallaApiKey".restartUnits = [svc];
    "caddy/email".restartUnits = [svc];
    "desecToken".restartUnits = [svc];
  };

  sops.templates.caddyPls = {
    owner = config.systemd.services.caddy.serviceConfig.User;
    content = ''
      (tlsCommon) {
        tls {
          # dns njalla ${p."caddy/njallaApiKey"}
          dns desec {
            token ${p.desecToken}
          }
          # propagation_timeout 1m
          propagation_timeout 2m
          # propagation_timeout -1
          curves x25519
          key_type p384
          protocols tls1.2 tls1.3
          # resolvers 8.8.8.8
        }
      }

      (headersCommon) {
        header / {
          x-frame-options "sameorigin"
          x-content-type-options "nosniff"
          x-xss-protection "1; mode=block"
          content-security-policy "
              upgrade-insecure-requests;
              default-src 'self';
              style-src 'self';
              script-src 'self';
              font-src 'self';
              img-src data: 'self';
              form-action 'self';
              connect-src 'self';
              frame-ancestors 'none';
          "
          cross-origin-opener-policy "same-origin"
          permissions-policy "geolocation=(), midi=(), sync-xhr=(), microphone=(), camera=(), magnetometer=(), gyroscope=(), fullscreen=(self), payment=()"
          referrer-policy "no-referrer; strict-origin-when-cross-origin"
          -Server
        }
      }

      (authentik) {
        # Always forward outpost path to actual outpost
        reverse_proxy /outpost.goauthentik.io/* http://localhost:9000

        # Forward authentication to outpost
        forward_auth http://localhost:9000 {
            uri /outpost.goauthentik.io/auth/caddy

            # Capitalization of the headers is important, otherwise they will be empty
            copy_headers X-Authentik-Username X-Authentik-Groups X-Authentik-Email X-Authentik-Name X-Authentik-Uid X-Authentik-Jwt X-Authentik-Meta-Jwks X-Authentik-Meta-Outpost X-Authentik-Meta-Provider X-Authentik-Meta-App X-Authentik-Meta-Version
        }
      }

      {
        admin off
        # acme_dns njalla ${p."caddy/njallaApiKey"}
        acme_dns desec {
          token ${p.desecToken}
        }
        email ${p."caddy/email"}
        grace_period 60s

        log default {
          output stdout
          format json
        }
      }

      *.${domain}, ${domain} {
      @authentik host auth.${domain}
      handle @authentik {
      # auth.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon

        # authelia
        # reverse_proxy localhost:9091

        # authentik
        reverse_proxy localhost:9000

        import headersCommon
      }

      @whoami host whoami.${domain}
      handle @whoami {
      # whoami.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon
        import headersCommon
        import authentik

        respond "I am whoami"
      }

      @gonic host gonic.${domain}
      handle @gonic {
      # gonic.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon
        import headersCommon
        # import authentik
        reverse_proxy localhost:4747
      }

      @ffsync host ffsync.${domain}
      handle @ffsync {
      # ffsync.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon
        import headersCommon
        # import authentik
        reverse_proxy localhost:${toString config.services.firefox-syncserver.settings.port}
      }

      # attic - nix cache.
      @cache host cache.${domain} nixcache.${domain}
      handle @cache {
      # cache.${domain} nixcache.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon
        import headersCommon
        # import authentik
        reverse_proxy localhost:5000
      }

      # uptime kuma
      @uptime host uptime.${domain}
      handle @uptime {
      # uptime.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon
        import headersCommon
        # import authentik
        reverse_proxy localhost:3001
      }

      # nextcloud
      @cloud host cloud.${domain}
      handle @cloud {
      # cloud.${domain} {
        encode zstd br
        log {
          level INFO
        }

        import tlsCommon
        import headersCommon
        # import authentik
        reverse_proxy localhost:8078 {
          transport http {
            tls
            tls_insecure_skip_verify
          }
        }
      }
      }
    '';
  };
}
