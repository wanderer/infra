{
  config,
  pkgs,
  sops-nix,
  ...
}: let
  svc = "authentik.service";
in {
  sops.secrets = {
    "authentik/secretKey".restartUnits = [svc];
    "authentik/emailHost".restartUnits = [svc];
    "authentik/emailUsername".restartUnits = [svc];
    "authentik/emailPassword".restartUnits = [svc];
    "authentik/emailFrom".restartUnits = [svc];
  };

  services.authentik = {
    enable = true;
    # The environmentFile needs to be on the target host!
    # Best use something like sops-nix or agenix to manage it
    environmentFile = config.sops.templates.authentikEnv.path;
    settings = {
      disable_startup_analytics = true;
      avatars = "initials";
      disable_update_check = true;
      error_reporting_enabled = false;
    };
  };

  sops.templates.authentikEnv = {
    content = ''
      AUTHENTIK_SECRET_KEY=${config.sops.placeholder."authentik/secretKey"}
      AUTHENTIK_EMAIL__HOST=${config.sops.placeholder."authentik/emailHost"}
      AUTHENTIK_EMAIL__USERNAME=${config.sops.placeholder."authentik/emailUsername"}
      AUTHENTIK_EMAIL__PASSWORD=${config.sops.placeholder."authentik/emailPassword"}
      AUTHENTIK_EMAIL__FROM=${config.sops.placeholder."authentik/emailFrom"}
      # AUTHENTIK_DISABLE_UPDATE_CHECK=true
      # AUTHENTIK_ERROR_REPORTING__ENABLED=false
    '';
  };
}
