{config, ...}: let
  svc = "atticd.service";
  p = config.sops.placeholder;
in {
  imports = [
    ../../../modules/attic.nix
  ];

  sops = {
    secrets = {
      "attic/serverToken".restartUnits = [svc];
    };
    templates.atticCreds = {
      content = ''
        ATTIC_SERVER_TOKEN_HS256_SECRET_BASE64="${p."attic/serverToken"}"
      '';
    };
  };

  services.atticd = {
    enable = true;
    credentialsFile = config.sops.templates.atticCreds.path;
    settings.listen = "127.0.0.1:5000";
  };
}
