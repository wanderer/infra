{
  config,
  disks ? ["/dev/sda"],
  lib,
  ...
}: let
  zfs-DATA = config.age.secrets.zfs-DATA;
in {
  disko.devices = {
    disk = {
      x = {
        type = "disk";
        device = "/dev/sda";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              type = "EF00";
              size = "700M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
          };
        };
      };
    };
    zpool = {
      zroot = {
        type = "zpool";
        mode = ""; # == single disk
        options = {
          ashift = "12";
          autotrim = "on";
        };
        rootFsOptions = {
          checksum = "sha512";
          compression = "zstd";
          "com.sun:auto-snapshot" = "false";
        };
        mountpoint = null;
        postCreateHook = "zfs snapshot zroot@blank";

        datasets = {
          "ROOT" = {
            type = "zfs_fs";
            mountpoint = null;
            options."com.sun:auto-snapshot" = "false";
          };
          "ROOT/nixos" = {
            type = "zfs_fs";
            mountpoint = "/";
            options."com.sun:auto-snapshot" = "true";
          };
          nix = {
            type = "zfs_fs";
            mountpoint = "/nix";
            options."com.sun:auto-snapshot" = "true";
          };
          #DATA = {
          #  type = "zfs_fs";
          #  options = {
          #    encryption = "aes-256-gcm";
          #    keyformat = "passphrase";
          #    keylocation = "file://${zfs-DATA.path}";
          #    mountpoint = "none";
          #    "com.sun:auto-snapshot" = "true";
          #  };
          #  # postCreateHook = ''
          #  # zfs set keylocation="file://${zfs-DATA}.path" "zroot/$name";
          #  # '';
          #};
        };
      };
    };
  };
}
