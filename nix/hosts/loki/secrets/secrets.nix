let
  # lokiage.
  user1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILKgwY12VnsfIKMNd0X/ZmevMdw2lEf1EUjbuxmmrsNb";
  users = [user1];

  system1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGzJL8/M+tTejrAPoomHKtlYk8lINBLHaH+p4SLt3sBG";
  systems = [system1];
in {
  "rootPassphrase.age".publicKeys = [user1];

  "zoneInternal.age".publicKeys = [user1];
  "zoneExternal.age".publicKeys = [user1];
  "corednsEnv.age".publicKeys = [user1];

  "autheliaStorage.age".publicKeys = [user1];
  "autheliaJWT.age".publicKeys = [user1];
  "autheliaEnv.age".publicKeys = [user1];
  "domainName.age".publicKeys = [user1];

  # "zfs-DATA.age".publicKeys = [user1 system1];
}
