{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./disko-config.nix

    ./modules/caddy.nix
    ./modules/coredns.nix
    # ./modules/authelia.nix
    ./modules/authentik.nix
    ./modules/gonic.nix
    ./modules/attic.nix
    ./modules/podman.nix

    ./modules/containers/nextcloud.nix

    ../../modules/base.nix
    ../../modules/dnscrypt.nix
    ../../modules/sanoid.nix
    ../../modules/uptime-kuma.nix
    ../../modules/zram.nix
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age = {
      keyFile = "/root/.age/loki-key";
      sshKeyPaths = ["/root/.ssh/lokiage" "/etc/ssh/ssh_host_ed25519_key"];
      generateKey = false;
    };

    secrets.domainName.restartUnits = ["caddy.service" "coredns.service"];
    secrets.nixServeSecretKey.restartUnits = ["nix-serve.service"];
  };

  age = {
    # `lokiage` key needs to be manually when setting up the machine;
    identityPaths = ["/root/.ssh/lokiage"];
    # identityPaths = ["/root/.ssh/lokiage" "/var/lib/persistent/ssh_host_ed25519_key"];

    secrets.rootPassphrase.file = ./secrets/rootPassphrase.age;
    # secrets."zfs-DATA".file = ./secrets/zfs-DATA.age;
  };

  nix.settings.trusted-users = ["@wheel" "root"];

  # forbid hibernation due to zfs-on-root.
  boot.kernelParams = ["nohibernate"];
  boot.kernel.sysctl = {
    "dev.i915.perf_stream_paranoid" = 0;
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 42;
  boot.loader.systemd-boot.netbootxyz.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.supportedFilesystems = ["zfs"];
  boot.zfs.forceImportRoot = true;

  boot.initrd.kernelModules = ["zfs" "e1000e"];
  boot.initrd.network = {
    # This will use udhcp to get an ip address.
    # Make sure you have added the kernel module for your network driver to `boot.initrd.availableKernelModules`,
    # so your initrd can load it!
    # Static ip addresses might be configured using the ip argument in kernel command line:
    # https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt
    enable = false;
    ssh = {
      enable = false;
      # To prevent ssh clients from freaking out because a different host key is used,
      # a different port for ssh is useful (assuming the same host has also a regular sshd running)
      port = 2222;
      # hostKeys paths must be unquoted strings, otherwise you'll run into issues with boot.initrd.secrets
      # the keys are copied to initrd from the path specified; multiple keys can be set
      # you can generate any number of host keys using
      # `ssh-keygen -t ed25519 -N "" -f /path/to/ssh_host_ed25519_key`
      # hostKeys = [/root/.initrd-ssh_host_ed25519_key];
      ignoreEmptyHostKeys = true;
      authorizedKeys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODmLwtQj6ylgdTPo1/H5jW7jsLzwaCTGdIsTQAdc896"];
    };
  };
  # boot.initrd.systemd.contents

  boot.binfmt = {
    emulatedSystems = [
      "wasm32-wasi"
      "aarch64-linux"
    ];
  };

  environment.systemPackages = with pkgs; [
    wol
  ];

  networking = {
    # hostId = pkgs.lib.mkForce "00000000";
    hostId = "deadb33f";
    hostName = "loki";

    nftables.enable = true;

    networkmanager.enable = true;
    interfaces.enp0s25.wakeOnLan.enable = true;

    firewall = {
      allowPing = true;
    };

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  };

  users.users.root = {
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBtG6NCgdLHX4ztpfvYNRaslKWZcl6KdTc1DehVH4kAL"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaXmXbNegxiXLldy/sMYX8kCsghY1SGqn2FZ5Jk7QJw"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBZbkw9vjCfbMPEH7ZAFq20XE9oIJ4w/HRIMu2ivNcej caelum's nixbldr key"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGKzPC0ZK4zrOEBUdu1KNThEleVb1T5Pl3+n3KB3o0b8 surtur's nixbldr key"
    ];
    hashedPasswordFile = config.age.secrets.rootPassphrase.path;
    subUidRanges = [
      {
        count = 65535;
        startUid = 65536 * 28; # 1835008, docker
      }
    ];
  };

  services = {
    atd.enable = true;

    udev.extraRules = ''
      # set brightness to minimum level.
      ACTION=="add", SUBSYSTEM=="backlight", ATTR{brightness}!="0", ATTR{brightness}="0"

      # wol
      ACTION=="add", SUBSYSTEM=="net", NAME=="en*", RUN+="${pkgs.ethtool}/bin/ethtool -s $name wol g"
    '';

    power-profiles-daemon.enable = false;
    #tlp.enable =
    #  lib.mkDefault ((lib.versionOlder (lib.versions.majorMinor lib.version) "23.11")
    #    || !config.services.power-profiles-daemon.enable);
    auto-cpufreq.enable = true;
    auto-cpufreq.settings = {
      battery = {
        governor = "powersave";
        turbo = "never";
      };
      charger = {
        governor = "schedutil";
        turbo = "auto";
      };
    };

    prometheus = {
      # WIP.
      enable = true;
      # openFirewall = true;
      port = 9090;
      exporters = {
        node = {
          enable = true;
          enabledCollectors = [
            "logind"
            "systemd"
          ];
          port = 9100;
        };
      };

      scrapeConfigs = [
        {
          job_name = "node";
          static_configs = [
            {
              targets = [
                "${config.networking.hostName}.local:${toString config.services.prometheus.exporters.node.port}"
              ];
            }
          ];
        }
        {
          job_name = "coredns";
          static_configs = [{targets = ["${config.networking.hostName}.local:9153"];}];
        }
      ];
    };

    sanoid = {
      enable = true;
      interval = "*:0/5";
      datasets = {
        "zroot/DATA/services/nextcloud" = {
          useTemplate = ["frequent"];
          recursive = "zfs";
        };
        "zroot/DATA/services/gonic" = {
          useTemplate = ["production"];
          recursive = "zfs";
        };
        "zroot/DATA/services/attic" = {
          useTemplate = ["production"];
          recursive = "zfs";
        };
        "zroot/DATA/services/uptime-kuma" = {
          useTemplate = ["production"];
          recursive = "zfs";
        };
        # rootfs.
        "zroot/ROOT/nixos" = {
          useTemplate = ["production"];
          recursive = "zfs";
        };
        # nix stuff, just to make sure.
        "zroot/nix" = {
          useTemplate = ["production"];
          recursive = "zfs";
        };
      };
    };

    # TS is enabled in the imported module, this is additional config.
    tailscale = {
      useRoutingFeatures = "both";
      # accept-routes = true;
    };

    zfs = {
      autoScrub = {
        enable = true;
        interval = "weekly";
      };
      trim.enable = true;
    };
  };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # Does not work with flakes - yet™.
  system.copySystemConfiguration = false;
}
