# [`infra/nix`](.)

## [:nixos: NixOS][nixos] configurations

* [`./hosts`](hosts) folder contains host-specific configurations
* [`./modules`](modules) folder contains reusable code

:rocket: deploy (build and switch to a new system) remotely using:
```sh
nixos-rebuild switch --fast --flake .#loki --target-host loki
```

where the *target host* `loki` is the `ssh-config` name of the host being
configured using the `.#loki` attribute of `nixosConfigurations`.

see `nixosConfigurations` attr in [`./flake.nix`](flake.nix) for a complete list of hosts.

[nixos]: https://nixos.org/
