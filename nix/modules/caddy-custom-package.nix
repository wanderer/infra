{
  lib,
  buildGoModule,
  fetchFromGitHub,
  plugins ? [],
  vendorSha256 ? "",
  pkgs,
}:
with lib; let
  imports = flip concatMapStrings plugins (pkg: "\t\t\t_ \"${pkg}\"\n");

  main = ''
    package main

    import (
      caddycmd "github.com/caddyserver/caddy/v2/cmd"

      ${imports}
      _ "github.com/caddyserver/caddy/v2/modules/standard"
    )

    func main() {
      caddycmd.Main()
    }
  '';
in
  pkgs.buildGo121Module rec {
    pname = "caddy";
    version = "2.7.4";
    # version = "latest";

    subPackages = ["cmd/caddy"];
    ldflags = [
      "-s -w"
    ];

    src = fetchFromGitHub {
      owner = "caddyserver";
      repo = pname;
      rev = "v${version}";
      sha256 = "sha256-oZSAY7vS8ersnj3vUtxj/qKlLvNvNL2RQHrNr4Cc60k=";
    };

    inherit vendorSha256;

    overrideModAttrs = _: {
      preBuild = "echo '${main}' > cmd/caddy/main.go && go mod tidy";
      postInstall = "cp go.sum go.mod $out/ && ls $out/";
    };

    postPatch = ''
      echo '${main}' > cmd/caddy/main.go
      cat cmd/caddy/main.go
    '';

    postConfigure = ''
      cp vendor/go.sum ./
      cp vendor/go.mod ./
    '';

    meta = with lib; {
      homepage = https://caddyserver.com;
      description = "Fast, cross-platform HTTP/2 web server with automatic HTTPS";
      license = licenses.asl20;
      maintainers = with maintainers; [rushmorem fpletz zimbatm];
    };
  }
