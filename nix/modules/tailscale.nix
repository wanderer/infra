{config, ...}: {
  services.tailscale.enable = true;

  networking.firewall = {
    trustedInterfaces = [config.services.tailscale.interfaceName];
    allowedUDPPorts = [config.services.tailscale.port];
    # specifically for Tailscale.
    checkReversePath = "loose";
  };
}
