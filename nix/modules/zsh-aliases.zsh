alias ls='eza -al --group --group-directories-first'
alias am='sudo -i'
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias free='free -m -l -t'
alias c='clear'
alias hs='history | grep -i'
alias neofetch='neofetch --disable "theme" "icons" packages de hostname cpu gpu resolution title'
alias wget='wget -c' # allows to restore progress after interruption
alias zshist="vim ~/.zsh_history"
alias agentssh="ssh-add ~/.ssh/$kee"
alias rsync-copy='rsync -avz --progress -h'
alias rsync-move='rsync -avz --progress -h --remove-source-files'
alias rsync-synchronize='rsync -avzu --delete --progress -h'
alias rsync-update='rsync -avzu --progress -h'
alias tmpdir='nutmp=$(mktemp -d);cd "$nutmp";unset nutmp'
alias ta='tmux a -t0'
alias tmux='TERM=xterm-256color tmux'
alias urlencode='python2 -c "import urllib, sys; print urllib.quote_plus(  sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1], \"\")"'
alias urldecode='python2 -c "import urllib, sys; print urllib.unquote_plus(sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1])"'
alias diff="diff -Naru --color"
alias ip='ip -color'
alias nmcs='nmcli conn show'
alias keyz='ssh-add -l'
alias gitagain='GIT_COMMITTER_DATE="$(git show -s --format=%ci HEAD)" git commit -v --amend'
[ -z "$IN_NIX_SHELL" ] && {
	if command -v nixGLIntel &> /dev/null; then
		alias gor='nixGLIntel go run'
	fi
}
alias signal-desktop='signal-desktop --use-tray-icon --enable-features=UseOzonePlatform --ozone-platform=wayland'
alias dotfiles="cd $dfHome"
alias -g VV=' |& vim -'
alias -g LL=' |& less'
alias normalise-unicode="python -c \"import unicodedata; import sys; arg=sys.argv[1]; print(unicodedata.normalize('NFKD', arg))\""
alias attic='nix run github:zhaofengli/attic'
