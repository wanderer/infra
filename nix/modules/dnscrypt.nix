{
  config,
  lib,
  ...
}: let
  usr = "dnscrypt-proxy";
  listenAddresses = [
    "127.0.0.1:53"
    "[::1]:53"
  ];
  disabledServerNames = [
    "google-ipv6"
    "cloudflare"
    "cloudflare-ipv6"
    "cisco"
    "cisco-ipv6"
    "cisco-familyshield"
    "cisco-familyshield-ipv6"
    "yandex"
    "apple"
    "doh.dns.apple.com"
    "ffmuc.net"
    # "dnswarden-uncensor-dc",
    # "dnswarden-uncensor-dc-swiss",
    # "techsaviours.org-dnscrypt",
    "dns.watch"
    "pryv8boi"
    "dct-at1"
    "dct-ru1"
    "dct-de1"
    # "dnscrypt.be",
    # "meganerd",
    "scaleway-ams"
    "scaleway-fr"
    "dnscrypt.pl"
    "acsacsar-ams-ipv4"
    "dnscrypt.uk-ipv4"
    "adguard-dns-unfiltered"
    "dnscry.pt-vienna-ipv4"
  ];
  bootstrapResolvers = [
    "9.9.9.9:53"
    "84.200.69.80:53"
    "84.200.70.40:53"
    "185.38.27.139:53"
    "130.226.161.34:53"
    # "[2a01:3a0:53:53::]:53"
    # "[2001:67c:28a4::]:53"
    # "[2001:1608:10:25::1c04:b12f]:53"
  ];
in {
  sops.secrets = {
    dnscrypt-proxy-forwardingRules = {
      sopsFile = ../secrets/dnscrypt-proxy.yaml;
      owner = usr;
      group = usr;
    };
  };

  services.dnscrypt-proxy2 = {
    enable = true;
    # don't go from scratch.
    upstreamDefaults = true;
    settings = {
      listen_addresses = listenAddresses;
      ipv4_servers = true;
      ipv6_servers = false;
      dnscrypt_servers = true;
      doh_servers = true;
      odoh_servers = false;
      require_dnssec = true;
      require_nolog = true;
      require_nofilter = true;
      disabled_server_names = disabledServerNames;
      http3 = true;
      timeout = 1000;
      keepalive = 30;
      lb_strategy = "p7";
      lb_estimator = true;
      log_level = 2;
      use_syslog = true;
      cert_refresh_delay = 60;
      bootstrap_resolvers = bootstrapResolvers;
      ignore_system_dns = true;
      # never timeout;
      netprobe_timeout = -1;
      netprobe_address = "9.9.9.9:53";
      # netprobe_address = "144.91.70.62:80";
      block_ipv6 = false;
      block_unqualified = true;
      # block_undelegated = true;
      block_undelegated = false;
      reject_ttl = 10;
      cache = true;
      cache_size = 10000;
      cache_min_ttl = 2400;
      cache_max_ttl = 86400;
      cache_neg_min_ttl = 60;
      cache_neg_max_ttl = 600;

      forwarding_rules = config.sops.secrets.dnscrypt-proxy-forwardingRules.path;

      sources.opennic = {
        urls = [
          "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/opennic.md"
          "https://download.dnscrypt.info/resolvers-list/v3/opennic.md"
        ];
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
        cache_file = "/var/cache/dnscrypt-proxy/opennic.md";
        refresh_delay = 24;
        prefix = "";
      };

      static."dotya.ml".stamp = "sdns://AQcAAAAAAAAAETE0NC45MS43MC42Mjo1NDQzIHF-JiN46cNwFXJleEVWGWgrhe2QeysUtZoo9HwzYCMzITIuZG5zY3J5cHQtY2VydC5kbnNjcnlwdC5kb3R5YS5tbA";

      #sources.public-resolvers = {
      #  urls = [
      #    "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
      #    "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
      #  ];
      #  cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
      #  minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      #};

      # You can choose a specific set of servers from https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v3/public-resolvers.md
      # server_names = [ ... ];
    };
  };

  systemd.services.dnscrypt-proxy2 = {
    after = ["sops-nix.service"];
    wants = ["coredns.service"];
    serviceConfig = {
      StateDirectory = usr;
      WorkingDirectory = "/";
      # StartLimitIntervalSec = 5;
      StartLimitBurst = 10;
      Restart = "always";
      RestartSec = 7;
      User = usr;
      Group = usr;
    };
  };

  users.users.dnscrypt-proxy = {
    group = usr;
    home = "/etc/" + usr;
    createHome = false;
    isSystemUser = true;
    extraGroups = ["users"];
  };
  users.groups.dnscrypt-proxy = {};
}
