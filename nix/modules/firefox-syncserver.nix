{
  config,
  pkgs,
  sops-nix,
  ...
}: let
  domain = p.domainName;
  d = p.shortDomain;
  p = config.sops.placeholder;
  svc = "firefox-syncserver.service";
in {
  # ref: https://nixos.org/manual/nixos/stable/#module-services-firefox-syncserver

  sops = {
    secrets = {
      "shortDomain" = {
        restartUnits = [svc];
      };
      "ffsync/masterSecret" = {
        restartUnits = [svc];
      };
      "ffsync/tokenserverMetricsHashSecret" = {
        restartUnits = [svc];
      };
    };
    templates = {
      ffsync-secrets = {
        content = ''
          SYNC_MASTER_SECRET=${p."ffsync/masterSecret"}
          SYNC_TOKENSERVER__FXA_METRICS_HASH_SECRET=${p."ffsync/tokenserverMetricsHashSecret"}
        '';
      };
    };
  };

  services.mysql.package = pkgs.mariadb;

  services.firefox-syncserver = {
    enable = true;
    secrets = config.sops.templates.ffsync-secrets.path;
    #secrets = builtins.toFile "sync-secrets" ''
    #  SYNC_MASTER_SECRET=this-secret-is-actually-leaked-to-/nix/store
    #'';
    database.createLocally = true;
    singleNode = {
      # autoconfigure.
      enable = true;
      hostname = "localhost";
      # hostname = "ffsync." + domain;
      # hostname = "ffsync." + d;
      # url = "https://ffsync." + d;
      # url = "https://ffsync." + domain;
      # url = "https://ffsync.${domain}";
      #url = "http://localhost:" + toString config.services.firefox-syncserver.settings.port;
      # url = "http://localhost:5000";
    };
    settings = {
      port = 5678;
      syncserver = {
        public_url = "https://ffsync.${domain}/";
        sqluri = "sqlite://///tmp/syncserver.db";
      };
      browserid = {
        backend = "tokenserver.verifiers.LocalVerifier";
        audiences = "https://ffsync.${domain}/";
      };
      tokenserver = {
        node_type = "sqlite";
      };
    };
  };
  systemd.services.firefox-syncserver.wants = ["sops-nix.service"];
}
