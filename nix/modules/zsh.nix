{pkgs, ...}: let
  shellInit = ''
    ZSH="$HOME/.oh-my-zsh"
    ZSH_CACHE_DIR="$ZSH/cache"
    ZSH_COMPDUMP="~/.zcompdump"
    ZSH_DISABLE_COMPFIX=true

    if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
      print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
      command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
      command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
    fi

    source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
    autoload -Uz _zinit
    (( ''${+_comps} )) && _comps[zinit]=_zinit

    # Load the pure theme, with zsh-async library that's bundled with it.
    zi ice pick"async.zsh" src"pure.zsh"
    zi light sindresorhus/pure

    # A glance at the new for-syntax - load all of the above
    # plugins with a single command. For more information see:
    # https://zdharma-continuum.github.io/zinit/wiki/For-Syntax/
    zinit for \
        light-mode \
      zsh-users/zsh-autosuggestions \
        light-mode \
      zdharma-continuum/fast-syntax-highlighting \
      zdharma-continuum/history-search-multi-word #\
        #light-mode \
        #pick"async.zsh" \
        #src"pure.zsh" \
        #sindresorhus/pure

    zinit ice wait"2" lucid # load after 2 seconds
    zinit load zdharma-continuum/history-search-multi-word
    zinit load sindresorhus/pure

    zi ice as"program" make'!' atclone'./direnv hook zsh > zhook.zsh' atpull'%atclone' src"zhook.zsh"
    zi light direnv/direnv

    plugins=(
      git
      gitignore
      golang
      # fzf
      terraform
      systemd
      safe-paste
      colored-man-pages
    )
    zi snippet OMZP::git
    zi snippet OMZP::gitignore
    zi snippet OMZP::golang
    # zi snippet OMZP::fzf
    zi snippet OMZP::systemd
    zi snippet OMZP::terraform
    zi snippet OMZP::safe-paste
    zi snippet OMZP::colored-man-pages

    HISTSIZE=10000
    SAVEHIST=10000
    setopt inc_append_history
    setopt extended_history # write the history file in the ":start:elapsed;command" format
    setopt share_history
    setopt hist_expire_dups_first # expire duplicate entries first when trimming history
    setopt hist_ignore_dups
    setopt hist_ignore_all_dups
    setopt hist_save_no_dups
    setopt hist_ignore_space
    setopt hist_reduce_blanks # remove superfluous blanks before recording entry
    setopt hist_verify # don't execute immediately after expansion

    # eval "$(starship init zsh)"
    # zinit load sindresorhus/pure

    setopt emacs
    # setopt autocd
    setopt \
      alwaystoend \
      autocd

    source ${./zsh-aliases.zsh}

    export TERM='xterm-256color'

    # make home-manager not manage the shell configuration
    HMVARSFILE="$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
    if [ -f "$HMVARSFILE" ]; then . "$HMVARSFILE"; fi

    PATH="$HOME/.cargo/bin:$HOME/go/bin:$PATH"

    zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
    zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
    zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
    zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
    # zstyle ':completion:*' group-name ""
    zstyle ':completion:*:*:-command-:*:*' group-order alias builtins functions commands
    # zstyle ':completion:*' file-list all
    zstyle ':completion:*' squeeze-slashes true
    zstyle ':completion:*:*:cp:*' file-sort modification
    # This way the completion script does not have to parse Bazel's options
    # repeatedly. The 'cache-path' directory in must be created manually.
    zstyle ':completion:*' use-cache on
    zstyle ':completion:*' cache-path $ZSH_CACHE_DIR
  '';
in {
  users.defaultUserShell = pkgs.zsh;

  programs.zsh.interactiveShellInit = shellInit;
  programs.zsh.loginShellInit = shellInit;
  programs.zsh.shellInit = shellInit;
}
