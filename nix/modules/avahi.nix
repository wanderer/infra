{
  config,
  pkgs,
  ...
}: {
  services = {
    avahi.enable = true;
    avahi.openFirewall = true;
    # avahi.nssmdns = true;
    avahi.nssmdns = false; # configure nssModules manually below.
    avahi.publish.enable = true;
    avahi.publish.userServices = true;
  };

  # settings from avahi-daemon.nix where mdns is replaced with mdns4
  system.nssModules = pkgs.lib.optional (!config.services.avahi.nssmdns) pkgs.nssmdns;
  system.nssDatabases.hosts = with pkgs.lib;
    optionals (!config.services.avahi.nssmdns) (mkMerge [
      (mkBefore ["mdns4 [NOTFOUND=return]"]) # before resolve
    ]);
}
