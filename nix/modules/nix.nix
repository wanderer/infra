{pkgs, ...}: {
  nix = {
    gc.automatic = true;
    gc.options = "--delete-older-than 30d";
    optimise.automatic = true;
    settings = {
      experimental-features = ["nix-command" "flakes" "recursive-nix"];
      keep-derivations = true;
      keep-outputs = true;
      auto-optimise-store = true;
      fallback = true;
      sandbox = true;
      trusted-public-keys = ["nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=  cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="];
      trusted-substituters = ["trusted-substituters = https://nix-community.cachix.org https://cache.nixos.org"];
    };
    package = pkgs.nixUnstable;
  };
}
