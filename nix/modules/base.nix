{
  lib,
  pkgs,
  ...
}: {
  imports = [
    ./avahi.nix
    ./nix.nix
    ./packages.nix
    ./tailscale.nix
    ./sysctl.nix
    ./zsh.nix
  ];

  boot.tmp.cleanOnBoot = true;

  environment = {
    variables = {
      EDITOR = "vim";
      PAGER = "less";
    };

    shells = with pkgs; [bash zsh];
  };

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    #   font = "Lat2-Terminus16";
    keyMap = "uk";
    #   useXkbConfig = true; # use xkbOptions in tty.
  };

  programs.zsh.enable = true;
  programs.zsh.enableCompletion = true;

  security = {
    rtkit.enable = true;
    sudo.extraConfig = "Defaults timestamp_timeout=300";
  };

  services = {
    earlyoom = {
      enable = true;
      enableNotifications = true;
      freeMemThreshold = 5;
    };

    openssh.enable = true;
    openssh.openFirewall = true;
  };

  systemd.extraConfig = "DefaultLimitNOFILE=1048576";

  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = lib.mkDefault "23.11";

  # Set your time zone.
  time.timeZone = lib.mkForce "Europe/Berlin";
}
