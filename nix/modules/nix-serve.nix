{config, ...}: {
  services.nix-serve = {
    enable = true;
    openFirewall = true;
    bindAddress = "127.0.0.1";
  };
}
