{
  config,
  lib,
  pkgs,
  ...
}: {
  services.sanoid = {
    templates = {
      production = {
        frequently = 5;
        hourly = 96;
        daily = 30;
        monthly = 3;
        yearly = 0;
        autosnap = true;
        autoprune = true;
      };

      frequent = {
        frequently = 90;
        frequent_period = 5;
        hourly = 96;
        daily = 30;
        monthly = 3;
        yearly = 0;
        autosnap = true;
        autoprune = true;
      };
    };
  };

  systemd.timers.sanoid.timerConfig = {
    Persistent = true;
    # WantedBy =["timers.target"];
  };

  environment.systemPackages = with pkgs; [
    mbuffer
    zstd
  ];

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrlXIj3oWbWrFUZnhccd/uqM2D/yRiKOEwfD6xbngJ7 syncoid pull backups"
  ];
}
