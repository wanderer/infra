{pkgs, ...}: {
  environment = {
    # https://github.com/NixOS/nixpkgs/issues/195795
    defaultPackages = [];
    # List packages installed in system profile. To search, run:
    # $ nix search wget
    systemPackages = with pkgs; [
      vim
      git
      curl
      wget
      rsync
      file
      gnused
      bat
      p7zip
      zstd
      b3sum

      bottom
      btop
      htop
      iotop

      dmidecode
      lsof
      tcpdump
      dnsutils
      netcat
      ethtool
      avahi

      nix-zsh-completions
      deadnix
      nil
      direnv

      sops
      age

      starship
      eza
      silver-searcher
      ripgrep
      zellij
      du-dust
      # dhall

      tailscale
    ];
  };
}
