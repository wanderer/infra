{...}: {
  services.authelia.instances = {
    main = {
      enable = true;
      settings = {
        theme = "dark";
        default_2fa_method = "totp";
        log.level = "debug";
        server.disable_healthcheck = false;
        telemetry.metrics.enabled = true;
      };
    };
  };
}
