#!/bin/sh

readonly vault_password_file_encrypted="$(dirname $0)/vault-$2-password.age"

flock "$vault_password_file_encrypted" \
	age -i ~/.age/ansible-vault-pw --decrypt "$vault_password_file_encrypted"
