# [`infra`][infra]

this repo holds the code describing my very own infra (machines I use/manage)
and is very much a WIP.

:nixos: [NixOS][nixos] configurations are present in the [`./nix`](nix) folder.

should contain zero secrets, except encrypted either with [`age`][age],
[`sops-nix`][sops-nix], or [`ansible-vault`][ansible-vault].

[`terraform`][tf] secrets are supplied as ENV vars at runtime by sourcing the
decrypted `infra-vars` file (stationed in its place with [`home-manager`][hm])
using [`direnv`][direnv].

[infra]: https://git.dotya.ml/wanderer/infra
[nixos]: https://nixos.org/
[age]: https://github.com/FiloSottile/age
[sops-nix]: https://github.com/Mic92/sops-nix
[ansible-vault]: https://docs.ansible.com/ansible/latest/cli/ansible-vault.html
[tf]: https://www.terraform.io/
[hm]: https://github.com/nix-community/home-manager
[direnv]: https://direnv.net/
