# https://www.tweag.io/blog/2019-04-03-terraform-provider-secret/
# resource "secret_resource" "" {}


##### provider block start
#

# https://www.linode.com/docs/guides/secrets-management-with-terraform/
provider "linode" {
  token = var.linode_token
}

provider "tailscale" {
  api_key = var.tailscale_api_key
  tailnet = var.tailscale_tailnet
}

#
##### provider block end

resource "linode_sshkey" "surtur" {
  label   = "nbgw"
  ssh_key = chomp(file("~/.ssh/surtur.pub"))
}

resource "linode_sshkey" "leo" {
  label   = "nbgw"
  ssh_key = chomp(file("~/.ssh/leo.pub"))
}

# perhaps switch this to Hetzner's CAX11, affords more bandwidth.
resource "linode_instance" "nbgw" {
  booted    = true
  region    = var.linode_region
  image     = "linode/fedora38"
  type      = "g6-standard-2"
  label     = "nbgw"
  group     = "Terraform"
  root_pass = var.linode_rootpasswd
  authorized_keys = [
    linode_sshkey.surtur.ssh_key,
    linode_sshkey.leo.ssh_key
  ]

  connection {
    type     = "ssh"
    user     = "root"
    password = var.linode_rootpasswd
    host     = self.ip_address
  }

  # remote-exec waits for the instance setup, so local-exec below will not fail immediatelly
  provisioner "remote-exec" {
    inline = ["/bin/true"]
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -e 'wheel_login=${var.linode_wheel_login} hostname=${self.label}' -u root -i '${self.ip_address},' ./ansible/playbooks/common.yml"
  }
}

output "nbgw-ipv4" {
  value = linode_instance.nbgw.ipv4
}

output "nbgw-ipv6" {
  value = linode_instance.nbgw.ipv6
}
