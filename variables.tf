variable "linode_token" {
  description = "Linode API token"
}

variable "linode_rootpasswd" {
  description = "Linode root passwd"
  type        = string
}

variable "linode_region" {
  description = "Linode region"
}

variable "linode_nbgw_hostname" {
  description = "nbgw hostname"
  type        = string
}

variable "linode_wheel_login" {
  type        = string
  description = "username of the admin user"
}

variable "tailscale_api_key" {
  type        = string
  description = "Tailscale API key"
}

variable "tailscale_tailnet" {
  type        = string
  description = "Tailscale Tailnet to manage"
}
